<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Http;

use Nette;

/**
 * URI Syntax (RFC 3986).
 *
 * <pre>
 * scheme  user  password  host  port  basePath   relativeUrl
 *   |      |      |        |      |    |             |
 * /--\   /--\ /------\ /-------\ /--\/--\/----------------------------\
 * http://john:x0y17575@nette.org:8042/en/manual.php?name=param#fragment  <-- absoluteUrl
 *        \__________________________/\____________/^\________/^\______/
 *                     |                     |           |         |
 *                 authority               path        query    fragment
 * </pre>
 *
 * - authority:   [user[:password]@]host[:port]
 * - hostUrl:     http://user:password@nette.org:8042
 * - basePath:    /en/ (everything before relative URI not including the script name)
 * - baseUrl:     http://user:password@nette.org:8042/en/
 * - relativeUrl: manual.php
 *
 * @author Karel
 *
 * @property-read string $basePath
 * @property-read string $baseName
 * @property-read string $extension
 * @property-read string $fileName
 * @property-read string $basePathArray
 * @property-read string $basePathBase64Decode
 * @property-read string $domain
 * @property-read string $domainArray
 * @property-write bool $sesurityOveride
 */
class Url extends Nette\Http\Url {

    /** @var string */
    private $basePath = '';

    /** @var string */
    private $baseName = '';

    /** @var string */
    private $extension = '';

    /** @var string */
    private $fileName = '';

    /** @var bool  */
    private $sesurityOveride = FALSE;

    /**
     * @param  string|self
     * @throws Nette\InvalidArgumentException if URL is malformed
     */
    public function __construct($url = NULL) {
        parent::__construct($url);

        $match = array();
        if (preg_match('%^(?P<basePath>/.*){0,1}/(?P<fileName>(?P<baseName>[0-9a-z_~-]*)([.](?P<extension>[0-9a-z]{0,5})){0,1})%i', $this->path, $match)) {
            if (isset($match['basePath'])) {
                $this->basePath = $match['basePath'];
            }
            if (isset($match['fileName'])) {
                $this->fileName = $match['fileName'];
            }
            if (isset($match['baseName'])) {
                $this->baseName = $match['baseName'];
            }
            if (isset($match['extension'])) {
                $this->extension = $match['extension'];
            }
        }
    }

    /**
     * Returns the base-path array.
     * @return array
     */
    public function getBasePathArray(): array {
        if (isset($this->basePath)) {
            return explode('/', trim($this->basePath, '/'));
        } else {
            return array();
        }
    }

    /**
     * Returns decoded base64 base-path.
     * @return string
     */
    public function getBasePathBase64Decode(): string {
        if (isset($this->basePath) && preg_match('%[a-z0-9+]*[=]{0,2}%', $this->basePath)) {
            $basePath = trim($this->basePath, '/');
            $decoded = trim(base64_decode($basePath, true), "\x80..\xFF");
            if (base64_encode($decoded) == $basePath) {
                return $decoded;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Returns name part of file.
     * @return string
     */
    public function getBaseName(): string {
        return $this->baseName;
    }

    /**
     * Returns the base-path.
     * @return string
     */
    public function getBasePath(): string {
        return $this->basePath;
    }

    /**
     * Returns extension of file.
     * @return string
     */
    public function getExtension(): string {
        return $this->extension;
    }

    /**
     * Returns filename.
     * @return string
     */
    public function getFileName(): string {
        return $this->fileName;
    }

    /**
     * Return host/domain to level
     * level 2 from www.nix.cz is nix.cz
     * @param int $level
     * @return string
     */
    public function getDomain(int $level = 2): string {
        $match = array();
        $return = '';
        preg_match('/(((((?P<level5>[^.]+)\.)?(?P<level4>[^.]+)\.)?(?P<level3>[^.]+)\.)?(?P<level2>[^.]+)\.)?(?P<level1>[^.]+)$/', $this->host, $match);
        for ($i = 1; !empty($match['level' . $i]) && $i <= 5 && $i <= $level; $i++) {
            $return = $match['level' . $i] . ((empty($return)) ? '' : '.') . $return;
        }
        return $return;
    }

    /**
     * Return host/domain as array parts
     * @return array
     */
    public function getDomainArray() {
        $match = array();
        $return = array();
        preg_match('/(((((?P<level5>[^.]+)\.)?(?P<level4>[^.]+)\.)?(?P<level3>[^.]+)\.)?(?P<level2>[^.]+)\.)?(?P<level1>[^.]+)$/', $this->host, $match);
        for ($i = 1; !empty($match['level' . $i]) && $i <= 5; $i++) {
            $return[$i] = $match['level' . $i];
        }
        return $return;
    }

    /**
     * Returns the [user[:pass]@]host[:port] part of URI.
     * @return string
     */
    public function getAuthority(): string {
        return $this->host === '' ? '' : ($this->user !== '' && (($this->scheme !== 'http' && $this->scheme !== 'https') || $this->sesurityOveride) ? rawurlencode($this->user) . ($this->password === '' ? '' : ':' . rawurlencode($this->password)) . '@' : '')
                . $this->host
                . ($this->port && (!isset(self::$defaultPorts[$this->scheme]) || $this->port !== self::$defaultPorts[$this->scheme]) ? ':' . $this->port : '');
    }

    /**
     * adds Username and password to http[s] protocol;
     * @param bool $sesurityOveride
     */
    function setSesurityOveride($sesurityOveride = FALSE) {
        $this->sesurityOveride = $sesurityOveride;
        return $this;
    }

}
