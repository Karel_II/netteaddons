<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI;

use Nette\Application\UI\Form,
    Nette\Forms\IFormRenderer;

/**
 *
 * @author Karel
 */
interface ISetupFormFactory {

    /**
     * 
     * @return Form Description
     */
    public function create(): Form;

    /**
     * Sets form renderer.
     * @return $this
     */
    public function setRenderer(IFormRenderer $renderer = null);
}
