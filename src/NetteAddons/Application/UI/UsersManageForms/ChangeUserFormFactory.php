<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI\UsersManageForms;

use \NetteAddons\Repository\UsersRepository,
    \Netteaddons\Security\Identity,
    \NetteAddons\Application\UI\BaseFormFactory,
    \NetteAddons\Application\UI\ISetupFormFactory;
use \Nette\Forms\Form,
    \Nette\Security\AuthenticationException;

/**
 * Description of ChangeUserFormFactory
 *
 * @author Karel
 */
class ChangeUserFormFactory extends BaseFormFactory {

    /**
     * 
     * @var \NetteAddons\Repository\UsersRepository 
     */
    private $userRepository;

    /** @var string  */
    protected $renderText = NULL;

    public function __construct(ISetupFormFactory $factory, UsersRepository $userRepository) {
        $this->userRepository = $userRepository;
        parent::__construct($factory);
    }

    /**
     * @return Form
     */
    public function createComponentForm() {
        $this->form->addHidden('id');
        $this->form->addText('username', 'Username :')->setHtmlAttribute('readonly');
        $this->form->addText('firstName', 'First Name :');
        $this->form->addText('middleName', 'Middle Name :');
        $this->form->addText('lastName', 'Last Name :');
        $this->form->addText('email', 'E-mail :');
        $this->form->addText('keyApi', 'API Key :');
        $this->form->addText('keyOtp', 'OTP Key :');
        $this->form->addButton('generateAPI', 'New API Key')
                ->setHtmlAttribute('onclick', 'getValue(this)')
                ->setHtmlAttribute('data-link', $this->link('generateApiKey!'))
                ->setHtmlAttribute('data-target', 'input[name=\'keyApi\']')
                ->setHtmlAttribute('class', 'btn btn-lg btn-success');
        $this->form->addButton('generateOTP', 'New OTP Key')
                ->setHtmlAttribute('onclick', 'getValue(this)')
                ->setHtmlAttribute('data-link', $this->link('generateOtpKey!'))
                ->setHtmlAttribute('data-target', 'input[name=\'keyOtp\']')
                ->setHtmlAttribute('class', 'btn btn-lg btn-success');
        $this->form->addSubmit('send', 'Change')
                ->setHtmlAttribute('class', 'btn-lg btn-success');

        $this->form->addProtection('Timeout, please resubmit your form !');
        $form = $this->form;
        $this->applyDefaults();
        $this->form->onSuccess[] = function (Form $form, $values) {
            try {
                $identity = new Identity($values->id, array(), (array) $values);
                $this->userRepository->identityUpdate($identity);
                $form->getPresenter()->flashMessage('Password has been changed', 'info');
            } catch (AuthenticationException $e) {
                $form->getPresenter()->flashMessage('Password cant be changed', 'danger');
                return;
            }
        };
        return $form;
    }

    public function setDefaults(Identity $identity) {
        if ($identity->id) {
            $this->defaults = $identity->data;
            $this->defaults['id'] = $identity->id;
            $this->applyDefaults();
        }
    }

    public function handleGenerateOtpKey() {
        if ($this->presenter->isAjax()) {
            $data['value'] = \Base32\Base32::base32_generate();
            $this->presenter->sendJson($data);
        }
        $this->presenter->redirect('Homepage:');
    }

    public function handleGenerateApiKey() {
        if ($this->presenter->isAjax()) {
            $data['value'] = \Nette\Utils\Random::generate(16, '0-9a-zA-Z');
            $this->presenter->sendJson($data);
        }
        $this->presenter->redirect('Homepage:');
    }

}
