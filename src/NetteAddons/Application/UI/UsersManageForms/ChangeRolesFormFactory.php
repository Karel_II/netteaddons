<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI\UsersManageForms;

use \NetteAddons\Repository\UsersRepository,
    \NetteAddons\Repository\RolesRepository,
    \NetteAddons\Repository\IdentityRolesRepository,
    \NetteAddons\Application\UI\BaseFormFactory,
    \NetteAddons\Application\UI\ISetupFormFactory,
    Netteaddons\Security\Identity;
use \Nette\Forms\Form,
    \Nette\Security\AuthenticationException;

/**
 * Description of ChangeRolesFormFactory
 *
 * @author karel.novak
 */
class ChangeRolesFormFactory extends BaseFormFactory {

    /**
     * 
     * @var \NetteAddons\Repository\UsersRepository 
     */
    private $userRepository;

    /**
     * 
     * @var \NetteAddons\Repository\IdentityRolesRepository 
     */
    private $identityRolesRepository;

    /**
     * 
     * @var \NetteAddons\Repository\RolesRepository 
     */
    private $rolesRepository;

    /** @var string  */
    protected $renderText = NULL;

    public function __construct(ISetupFormFactory $setupFormFactory, UsersRepository $userRepository, RolesRepository $rolesRepository, IdentityRolesRepository $identityRolesRepository) {
        $this->userRepository = $userRepository;
        $this->rolesRepository = $rolesRepository;
        $this->identityRolesRepository = $identityRolesRepository;
        parent::__construct($setupFormFactory);
    }

    public function render() {
        $this->template->userRoles = $this->items['userRole'];
        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm() {
        $this->form->addHidden('id');

        $this->form->addRadioList("action", 'Action :', array(
                    1 => "Add Role:",
                    2 => "Change Role:",
                    3 => "Delete Role:"
                ))
                ->setDefaultValue(1)
                ->addCondition(Form::EQUAL, 1)
                ->toggle('frm-changeRolesForm-role', TRUE)
                ->toggle('frm-changeRolesForm-order', TRUE)
                ->endCondition()
                ->addCondition(Form::EQUAL, 2)
                ->toggle('frm-changeRolesForm-userRole', TRUE)
                ->toggle('frm-changeRolesForm-order', TRUE)
                ->endCondition()
                ->addCondition(Form::EQUAL, 3)
                ->toggle('frm-changeRolesForm-userRole', TRUE)
                ->endCondition();

        $this->form->addSelect('role', 'Role :');
        $this->form->addSelect('userRole', 'User Role :');
        $this->form->addText('order', 'Order :');

        $this->form->addSubmit('send', 'Change')
                ->setHtmlAttribute('class', 'btn-lg btn-success');
        $this->form->addProtection('Timeout, please resubmit your form !');
        $form = $this->form;
        $this->applyDefaults();
        $this->applyItems(FALSE);
        $this->form->onSuccess[] = function (Form $form, $values) {
            $this->changeRolesSuccess($form, $values);
        };

        return $form;
    }

    private function changeRolesSuccess(Form $form, $values) {
        $identity = new \Netteaddons\Security\Identity($values->id);

        try {
            switch ($values->action) {
                case 1:
                    $this->identityRolesRepository->idtentityRoleAdd($identity, $values->role, $values->order);
                    break;
                case 2:
                    $this->identityRolesRepository->idtentityRoleMove($identity, $values->userRole, $values->order);
                    break;
                case 3:
                    $this->identityRolesRepository->idtentityRoleDelete($identity, $values->userRole);
                    break;
                default :
                    $form->getPresenter()->flashMessage('Role cant be changed', 'danger');
                    return;
            }
            $form->getPresenter()->flashMessage('Role has been changed', 'info');
        } catch (AuthenticationException $e) {
            $form->getPresenter()->flashMessage('Role cant be changed', 'danger');
            return;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setDefaults(Identity $identity) {
        if ($identity->id) {
            $this->defaults = $identity->data;
            $this->defaults['id'] = $identity->id;
            $this->items = array();
            $this->items['role'] = $this->rolesRepository->getRoles();
            $this->items['userRole'] = $identity->roles;
            $this->applyItems();
            $this->applyDefaults();
        }
    }

}
