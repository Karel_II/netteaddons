<?php

namespace NetteAddons\Application\UI\UsersManageForms;

use \NetteAddons\Repository\UsersRepository,
    \Netteaddons\Security\UserManager,
    \Netteaddons\Security\Identity,
    \NetteAddons\Application\UI\BaseFormFactory,
    \NetteAddons\Application\UI\ISetupFormFactory;
use \Nette\Forms\Form,
    \Nette\Security\AuthenticationException;

/**
 * Description of ChangePasswordFormFactory
 *
 * @author karel.novak
 */
class ChangePasswordFormFactory extends BaseFormFactory {

    /**
     * @var \NetteAddons\Repository\UsersRepository 
     */
    private $userRepository;

    /**
     * @param ISetupFormFactory $setupFormFactory
     * @param UsersRepository $userRepository
     */
    public function __construct(ISetupFormFactory $setupFormFactory, UsersRepository $userRepository) {
        $this->userRepository = $userRepository;
        parent::__construct($setupFormFactory);
    }

    /**
     * @return Form
     */
    public function createComponentForm() {
        $this->form->addPassword('password')
                ->setHtmlAttribute('placeholder', 'Password')
                ->setOption('description', sprintf('at least %d characters', UserManager::PASSWORD_MIN_LENGTH))
                ->setRequired('Please enter your password.')
                ->addRule(Form::MIN_LENGTH, NULL, UserManager::PASSWORD_MIN_LENGTH);
        $this->form->addPassword('passwordVerify')
                ->setHtmlAttribute('placeholder', 'Retype password')
                ->setRequired('Please enter your password again.')
                ->addRule(Form::EQUAL, 'Passwords do not match', $this->form['password']);
        $this->form->addHidden('id');
        $this->form->addSubmit('send', 'Change Password')
                ->setHtmlAttribute('class', 'btn btn-lg btn-success');
        $this->form->addProtection('Timeout, please resubmit your form !');
        $form = $this->form;
        $this->applyDefaults();
        $this->applyItems();
        $this->form->onSuccess[] = function (Form $form, $values) {
            try {
                $identity = new Identity($values->id, array(), (array) $values);
                $this->userRepository->identityUpdatePassword($identity, $values->password);
                $form->getPresenter()->flashMessage('Password has been changed', 'info');
            } catch (AuthenticationException $e) {
                $form->getPresenter()->flashMessage('Password could not be changed.', 'danger');
                return;
            }
        };
        return $form;
    }

    public function setDefaults(Identity $identity) {
        if ($identity->id) {
            $this->defaults = $identity->data;
            $this->defaults['id'] = $identity->id;
            $this->applyDefaults();
        }
    }

}
