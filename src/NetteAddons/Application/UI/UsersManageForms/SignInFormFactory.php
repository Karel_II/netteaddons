<?php

namespace NetteAddons\Application\UI\UsersManageForms;

use \NetteAddons\Application\UI\BaseFormFactory,
    \NetteAddons\Application\UI\ISetupFormFactory;
use \Nette\Forms\Form,
    \Nette\Security\User,
    \Nette\Security\AuthenticationException;

/**
 * Description of SignUpFormFactory
 *
 * @author karel.novak
 */
class SignInFormFactory extends BaseFormFactory {

    /**
     * 
     * @var \Nette\Security\User 
     */
    private $user;

    /**
     * 
     * @param ISetupFormFactory $setupFormFactory
     * @param User $user
     */
    public function __construct(ISetupFormFactory $setupFormFactory, User $user) {
        $this->user = $user;
        parent::__construct($setupFormFactory);
    }

    /**
     * 
     * @return Form
     */
    public function createComponentForm() {
        $this->form->addText('username')
                ->setHtmlAttribute('placeholder', 'Username')
                ->setRequired('Please enter your username.');
        $this->form->addPassword('password')
                ->setHtmlAttribute('placeholder', 'Password')
                ->setRequired('Please enter your password.');
        $this->form->addText('otp')
                ->setHtmlAttribute('placeholder', 'One Time Password');
        $this->form->addCheckbox('remember', 'Keep me signed in');
        $this->form->addSubmit('send', 'Sign in')
                ->setHtmlAttribute('class', 'btn-lg btn-success');
        $form = $this->form;
        $this->applyDefaults();
        $this->applyItems();
        $this->form->onSuccess[] = function (Form $form, $values) {
            try {
                $this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
                $this->user->login($values->username, serialize(array("password" => $values->password, "otp" => $values->otp)));
            } catch (AuthenticationException $e) {
                $form->addError('The username or password you entered is incorrect.');
                return;
            }
        };
        return $form;
    }

}
