<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI\UsersManageForms;

use \NetteAddons\Repository\UsersRepository,
    \Netteaddons\Security\Identity,
    \NetteAddons\Application\UI\BaseFormFactory,
    \NetteAddons\Application\UI\ISetupFormFactory;
use \Nette\Forms\Form;

/**
 * Description of ForgotPasswordFormFactory
 *
 * @author karel.novak
 */
class ForgotPasswordFormFactory extends BaseFormFactory {

    /**
     * 
     * @var \NetteAddons\Repository\UsersRepository 
     */
    private $userRepository;

    /** @var callable  function ($otp, \Netteaddons\Security\Identity $identity); Call Mail Sender routine */
    private $onSendEmail = NULL;

    /**
     * @param ISetupFormFactory $setupFormFactory
     * @param UsersRepository $userRepository
     */
    public function __construct(ISetupFormFactory $setupFormFactory, UsersRepository $userRepository) {
        $this->userRepository = $userRepository;
        parent::__construct($setupFormFactory);
    }

    /**
     * 
     * @param callable  function ($otp, \Netteaddons\Security\Identity $identity); Call Mail Sender routine
     * @return $this
     */
    public function setOnSendEmail(callable $onSendEmail) {
        $this->onSendEmail = $onSendEmail;
        return $this;
    }

    /**
     * @return Form
     */
    public function createComponentForm() {
        $this->form->addText('email', 'Enter e-mail address:')
                ->setRequired('Please enter your e-mail address.');

        $this->form->addSubmit('send', 'Send Request');
        $this->form->addProtection('Timeout, please resubmit your form !');

        $form = $this->form;
        $this->applyDefaults();
        $this->applyItems();
        $onSendEmail = $this->onSendEmail;
        $this->form->onSuccess[] = function (Form $form, $values) {
            $identity = new Identity(0, array(), ['email' => $values->email]);
            if (($otp = $this->userRepository->identityAddOtpPasswordChange($identity)) && is_callable($onSendEmail)) {
                $form->getPresenter()->flashMessage('Email has been sent to ' . $identity->email);
                $onSendEmail($otp, $identity);
            } else {
                $form->getPresenter()->flashMessage('Email could not been sent to ' . $identity->email);
            }
        };
        return $form;
    }

    public function setDefaults(Identity $identity) {
        if ($identity->id) {
            $this->defaults = $identity->data;
            $this->defaults['id'] = $identity->id;
            $this->applyDefaults();
        }
    }

}
