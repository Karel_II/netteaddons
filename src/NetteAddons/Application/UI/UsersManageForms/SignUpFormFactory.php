<?php

namespace NetteAddons\Application\UI\UsersManageForms;

use \NetteAddons\Repository\UsersRepository,
    \Netteaddons\Security\UserManager,
    \NetteAddons\Application\UI\BaseFormFactory,
    \NetteAddons\Application\UI\ISetupFormFactory;
use \Nette\Forms\Form;
use \NetteAddons\UserNameDuplicateException;

/**
 * Description of SignUpFormFactory
 *
 * @author karel.novak
 */
class SignUpFormFactory extends BaseFormFactory {

    /**
     * @var \NetteAddons\Repository\UsersRepository 
     */
    private $userRepository;

    /**
     * @param ISetupFormFactory $setupFormFactory
     * @param UsersRepository $userRepository
     */
    public function __construct(ISetupFormFactory $setupFormFactory, UsersRepository $userRepository) {
        $this->userRepository = $userRepository;
        parent::__construct($setupFormFactory);
    }

    /**
     * 
     * @param callable $onSuccess
     * @return Form
     */
    public function createComponentForm() {
        $this->form->addText('username')
                ->setHtmlAttribute('placeholder', 'Pick a username')
                ->setRequired('Please pick a username.');
        $this->form->addText('email')
                ->setHtmlAttribute('placeholder', 'Your e-mail')
                ->setRequired('Please enter your e-mail.')
                ->addRule(Form::EMAIL);
        $this->form->addPassword('password')
                ->setHtmlAttribute('placeholder', 'Create a password')
                ->setOption('description', sprintf('at least %d characters', UserManager::PASSWORD_MIN_LENGTH))
                ->setRequired('Please create a password.')
                ->addRule(Form::MIN_LENGTH, NULL, UserManager::PASSWORD_MIN_LENGTH);
        $this->form->addSubmit('send', 'Sign up')
                ->setHtmlAttribute('class', 'btn-lg btn-success');
        $form = $this->form;
        $this->applyDefaults();
        $this->applyItems();
        $this->form->onSuccess[] = function (Form $form, $values) {
            try {
                $this->userRepository->addUser($values->username, $values->email, $values->password, 'user');
            } catch (UserNameDuplicateException $e) {
                $form->addError('Username is already taken.');
                return;
            }
        };
        return $form;
    }

}
