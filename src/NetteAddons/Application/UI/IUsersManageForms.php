<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI;

use NetteAddons\Application\UI\UsersManageForms;

interface IChangePasswordFormFactory {

    /** @return \NetteAddons\Application\UI\UsersManageForms\ChangePasswordFormFactory */
    function create(): UsersManageForms\ChangePasswordFormFactory;
}

interface IChangeRolesFormFactory {

    /** @return \NetteAddons\Application\UI\UsersManageForms\ChangeRolesFormFactory */
    function create(): UsersManageForms\ChangeRolesFormFactory;
}

interface IChangeUserFormFactory {

    /** @return \NetteAddons\Application\UI\UsersManageForms\ChangeUserFormFactory */
    function create(): UsersManageForms\ChangeUserFormFactory;
}

interface IForgotPasswordFormFactory {

    /** @return \NetteAddons\Application\UI\UsersManageForms\ForgotPasswordFormFactory */
    function create(): UsersManageForms\ForgotPasswordFormFactory;
}

interface ISignInFormFactory {

    /** @return \NetteAddons\Application\UI\UsersManageForms\SignInFormFactory */
    function create(): UsersManageForms\SignInFormFactory;
}

interface ISignUpFormFactory {

    /** @return  \NetteAddons\Application\UI\UsersManageForms\SignUpFormFactory */
    function create(): UsersManageForms\SignUpFormFactory;
}
