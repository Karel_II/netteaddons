<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI;

use Nette\Application\UI\Template;

/**
 * Description of BaseControl
 *
 * @author Karel_II
 */
abstract class BaseComponentControl extends \Nette\Application\UI\Control {

    /**
     *
     * @var string 
     */
    protected $view = 'default';

    /**
     *
     * @var array 
     */
    protected $templateFiles = array();

    /**
     *
     * @var Nette/Localization/ITranslator
     */
    protected $translator = NULL;

    /**
     * Return new subcontrol
     * @return \static
     * @deprecated
     */
    final protected function createComponentStatic() {
        $static = new static;
        if ($this->translator) {
            $static->setTranslator($this->translator);
        }
        return $static;
    }

    /**
     * Return new subcontrol
     * @return $this
     */
    protected function createComponentThis() {
        return clone $this;
    }

    /**
     * 
     * @param string $functionName __FUNCTION__
     */
    protected function setViewFromFunctionName($functionName) {
        $m = array();
        preg_match('#render(\w+)$#', $functionName, $m);
        if (isset($m[1]) && !empty($m[1])) {
            $view = lcfirst($m[1]);
        } else {
            $view = 'default';
        }

        if (strcmp($view, $this->view) !== 0) {
            $this->view = $view;
        }
    }

    protected function controlName() {
        $m = array();
        preg_match('#(\w+)Control$#', get_class($this), $m);
        if (isset($m[1])) {
            return $m[1];
        }
    }

    /**
     * Formats view template file names.
     * @return array
     */
    public function formatTemplateFiles() {
        $control = $this->controlName();
        $dir = dirname($this->getReflection()->getFileName());
        $dir = is_dir("$dir/templates") ? $dir : dirname($dir);
        return array(
            "$dir/templates/$control/$this->view.latte",
            "$dir/templates/$control.$this->view.latte",
            "$dir/templates/$control/$this->view.phtml",
            "$dir/templates/$control.$this->view.phtml",
        );
    }

    protected function createTemplate(/* string $class = null */): Template {
        $template = parent::createTemplate();



        if (isset($this->templateFiles[$this->view])) {
            $template->setFile($this->templateFiles[$this->view]);
        } else { // content template
            $files = $this->formatTemplateFiles();
            foreach ($files as $file) {
                if (is_file($file)) {
                    $this->templateFiles[$this->view] = $file;
                    $template->setFile($file);
                    break;
                }
            }

            if (!$template->getFile()) {
                $file = preg_replace('#^.*([/\\\\].{1,70})\z#U', "\xE2\x80\xA6\$1", reset($files));
                $file = strtr($file, '/', DIRECTORY_SEPARATOR);
                throw new \Exception("Page not found. Missing template '$file'.");
            }
        }
        if ($this->translator) {
            $template->setTranslator($this->translator);
        }
        return $template;
    }

    /**
     * 
     * @param Nette/Localization/ITranslator $translator
     */
    public function setTranslator($translator) {
        $this->translator = $translator;
    }

}
