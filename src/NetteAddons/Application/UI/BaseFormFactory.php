<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Application\UI;

use Nette;
use \Nette\Application\UI\Form,
    \Nette\Application\UI\Control;
use Nette\Application\UI\Template;

/**
 * Description of BaseFormFactory
 *
 * @author karel.novak
 */
abstract class BaseFormFactory extends Control {

    /** @var  ISetupFormFactory */
    protected $setupFormFactory;

    /**
     * @var \Nette\Application\UI\Form 
     */
    protected $form;

    /** @var Nette/Localization/ITranslator */
    protected $translator = NULL;

    /** @var array */
    protected $defaults = null;

    /** @var array */
    protected $items = null;

    /** @var callable[]  function (Form $sender); Occurs when the form is submitted and successfully validated */
    public $onSuccess;

    /** @var callable[]  function (Form $sender); Occurs when the error is raised */
    public $onError;

    /**
     * Set NULL to load from file.
     * @var string */
    protected $renderText = '{control form}';

    /**
     * 
     * @param ISetupFormFactory $setupFormFactory
     */
    public function __construct(ISetupFormFactory $setupFormFactory) {
        $this->setupFormFactory = $setupFormFactory;
        $this->form = $this->setupFormFactory->create();
        $this->defaults = array();
        $this->items = array();
    }

    /**
     * 
     * @param Nette/Localization/ITranslator $translator
     */
    public function setTranslator($translator) {
        $this->translator = $translator;
    }

    /**
     * 
     * @return \Nette\Application\UI\Form 
     */
    public function getForm() {
        return $this->form;
    }

    /**
     * 
     */
    public function render() {
        $latteLoader = $this->template->getLatte()->getLoader();
        if ($latteLoader instanceof \Latte\Loaders\FileLoader) {
            $this->template->render();
        } elseif ($latteLoader instanceof \Latte\Loaders\StringLoader) {
            $this->template->render($this->renderText);
        } else {
            throw new \Exception;
        }
    }

    /**
     * 
     * @param string $name
     * @return Form
     */
    protected function createComponent(string $name): ?Nette\ComponentModel\IComponent {
        $component = parent::createComponent($name);
        if ($component instanceof Form) {
            if ($this->translator) {
                $component->setTranslator($this->translator);
            }
            $this->attachOnSuccess($component);
            $this->attachOnError($component);
        }
        return $component;
    }

    /**
     * 
     * @return boolean
     */
    protected function applyDefaults() {
        if (is_array($this->defaults) && isset($this->form) && !empty($this->form->components)) {
            $this->form->setDefaults($this->defaults);
            return true;
        }
        return false;
    }

    /**
     * 
     * @return boolean
     */
    protected function applyItems($useKeys = TRUE) {
        if (is_array($this->items) && isset($this->form) && !empty($this->form->components)) {
            foreach ($this->items as $name => $items) {
                if (($component = $this->form->getComponent($name, FALSE)) !== NULL) {
                    $component->setItems($items, $useKeys);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 
     * @param Form $form
     * @throws Nette\UnexpectedValueException
     */
    protected function attachOnSuccess(Form $form) {
        if ($this->onSuccess !== NULL) {
            if (!is_array($this->onSuccess) && !$this->onSuccess instanceof \Traversable) {
                throw new Nette\UnexpectedValueException('Property Form::$onSuccess must be array or Traversable, ' . gettype($this->onSuccess) . ' given.');
            }
            foreach ($this->onSuccess as $handler) {
                $form->onSuccess[] = $handler;
            }
        }
    }

    /**
     * 
     * @param Form $form
     * @throws Nette\UnexpectedValueException
     */
    protected function attachOnError(Form $form) {
        if ($this->onError !== NULL) {
            if (!is_array($this->onError) && !$this->onError instanceof \Traversable) {
                throw new Nette\UnexpectedValueException('Property Form::$onError must be array or Traversable, ' . gettype($this->onError) . ' given.');
            }
            foreach ($this->onError as $handler) {
                $form->onError[] = $handler;
            }
        }
    }

    /**
     * return Form
     */
    abstract public function createComponentForm();

    protected function controlName() {
        $m = array();
        preg_match('#(\w+)(Control|Factory)$#', get_class($this), $m);
        if (isset($m[1])) {
            return lcfirst($m[1]);
        }
    }

    /**
     * Formats view template file names.
     * @return array
     */
    private function formatTemplateFiles() {
        $control = $this->controlName();
        $dir = dirname($this->getReflection()->getFileName());
        $dir = is_dir("$dir/templates") ? $dir : dirname($dir);
        return array(
            "$dir/templates/$control.latte",
            "$dir/templates/$control.phtml",
        );
    }

    /**
     * @return ITemplate
     * @throws \Exception
     */
    protected function createTemplate(/* string $class = null */): Template {
        $template = parent::createTemplate();


        $latte = $template->getLatte();
        if (is_null($this->renderText) && !$template->getFile()) { // content template
            $latte->setLoader(new \Latte\Loaders\FileLoader());
            $files = $this->formatTemplateFiles();
            foreach ($files as $file) {
                if (is_file($file)) {
                    $template->setFile($file);
                    break;
                }
            }
            if (!$template->getFile()) {
                $file = preg_replace('#^.*([/\\\\].{1,70})\z#U', "\xE2\x80\xA6\$1", reset($files));
                $file = strtr($file, '/', DIRECTORY_SEPARATOR);
                throw new \Exception("Page not found. Missing template '$file'.");
            }
        } elseif (is_string($this->renderText)) {
            $latte->setLoader(new \Latte\Loaders\StringLoader);
        }

        if ($this->translator) {
            $template->setTranslator($this->translator);
        }
        return $template;
    }

}
