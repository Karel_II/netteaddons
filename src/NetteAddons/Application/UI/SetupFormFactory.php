<?php

namespace NetteAddons\Application\UI;

use Nette\Application\UI\Form,
    Nette\Forms\IFormRenderer,
    Nette\ComponentModel\IContainer;

final class SetupFormFactory implements ISetupFormFactory {

    private $renderer = NULL;

    /**
     * @return Form
     */
    public function create(IContainer $parent = NULL, $name = NULL): Form {
        $form = new Form($parent, $name);
        if ($this->renderer instanceof \Nette\Forms\IFormRenderer) {
            $form->setRenderer($this->renderer);
        }
        return $form;
    }

    /**
     * Sets form renderer.
     * @return $this
     */
    public function setRenderer(IFormRenderer $renderer = null) {
        $this->renderer = $renderer;
        return $this;
    }
}
