<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

/**
 * Description of Math
 *
 * @author karel.novak
 */
class Math {

    const BITS_PER_BYTE = 8;

    private static function recalculateShifts($shifts, $int_size = PHP_INT_SIZE) {
        $php_int_bits = PHP_INT_SIZE * static::BITS_PER_BYTE;
        $set_int_bits = $int_size * static::BITS_PER_BYTE;
        if (abs($shifts) > $set_int_bits) {
            return $php_int_bits * (int) ($shifts / $set_int_bits) + ($shifts % $set_int_bits);
        } else {
            return $shifts;
        }
    }

    /**
     * 
     * @param int $value
     * @param int $shifts
     * @param int $int_size
     * @return int
     * @throws \Exception
     * 
     */
    public static function unsignedShiftRight($value, $shifts, $int_size = PHP_INT_SIZE) {
        $set_int_bits = $int_size * static::BITS_PER_BYTE;

        if ($int_size % 4 !== 0 || $int_size > PHP_INT_SIZE) {
            throw new \Exception('Wrong Integer size value !');
        }
        if ($value > PHP_INT_MAX || $value > pow(2, $set_int_bits)) {
            throw new \Exception('Value ' . $value . ' is too large, maximum is ' . pow(2, $set_int_bits));
        }
        if ($shifts == 0) {
            return $value;
        }
        $steps = self::recalculateShifts($shifts, $int_size);
        return ($value >> $steps) & ~(PHP_INT_MAX << ($set_int_bits - 1) >> ($steps - 1));
    }

    /**
     * 
     * @param int $start
     * @param int $stop
     * @param int $amount
     * @return int
     */
    public static function lengthRepeatValue($start, $stop, $amount) {
        return $start + ($stop - $start) * $amount;
    }

    public static function mapValue($value, $minValue, $maxValue, $minOutput, $maxOutput) {
        return $minOutput + ($maxOutput - $minOutput) * (($value - $minValue) / ($maxValue - $minValue));
    }

}
