<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

use Nette\Utils\Validators,
    Nette\Security\Identity;

/**
 *
 * @author Karel
 * 
 * @property bool|int|null $disable Description
 * @property-read bool $disabled Description
 * @property-read bool $enabled Description
 */
trait TObjectDisable {

    /**
     * NULL or FALSE = enabled; >=0 of TRUE = disabled
     * @var int|bool 
     */
    private $disable = FALSE;

    /**
     * Enable
     * @return $this
     */
    public function enable() {
        $this->disable = FALSE;
        return $this;
    }

    /**
     * Disable 
     * @return $this
     */
    public function disable() {
        $this->disable = TRUE;
        return $this;
    }

    /**
     * 
     * @return int|null
     */
    public function getDisable() {
        if (Validators::is($this->disable, 'int:0..')) {
            return $this->disable;
        } elseif ($this->disable == TRUE) {
            return IObjectDisable::DISABLE_IDENTITY_ID;
        }
        return NULL;
    }

    /**
     * 
     * @return bool|\Nette\Security\Identity
     */
    public function getDisableIdentity() {
        if (Validators::is($this->disable, 'int:0..')) {
            return new Identity($this->disable);
        } elseif ($this->disable == TRUE) {
            return new Identity(IObjectDisable::DISABLE_IDENTITY_ID);
        }
        return NULL;
    }

    /**
     * 
     * @param null|bool|int|\Nette\Security\Identity $disable
     * @return $this
     */
    public function setDisable($disable = false) {
        Validators::assert($disable, 'null|bool|int:0..|\Nette\Security\Identity', '$disable');
        if (Validators::is($disable, 'bool')) {
            $this->disable = ($disable) ? TRUE : NULL;
        } elseif (Validators::is($disable, 'null|int:0..')) {
            $this->disable = $disable;
        } elseif (Validators::is($disable, '\Nette\Security\Identity')) {
            $this->disable = $disable->id;
        }
        return $this;
    }

    /**
     * 
     * @return bool
     */
    public function isDisabled() {
        return ($this->disable === TRUE || Validators::is($this->disable, 'int:0..')) ? TRUE : FALSE;
    }

    /**
     * 
     * @return bool
     */
    public function isEnabled() {
        return ($this->disable === TRUE || Validators::is($this->disable, 'int:0..')) ? FALSE : TRUE;
    }

}
