<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

/**
 *
 * @author karel.novak
 */
interface IObjectId {

    /**
     * Return Object Id
     * @return mixed
     */
    public function getId();

    /**
     * Return true if Object id has been set !
     * @return bool
     */
    public function hasId();

    /**
     * Object Id must be scalar !
     * @param mixed
     * @throws \NetteAddons\ObjectIdException
     */
    public function setId($id);

    public static function newObjectId($id);
}
