<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

use Nette\Application\UI\ComponentReflection;

/**
 * Description of ObjectArrayTrait
 *
 * @author Karel
 * 
 * magic methods for @toArray, @fromArray annotation
 * 
 * usage 
 * @toArray(:challengePassword,configurationFile:challengePassword)
 * @fromArray(:challengePassword,configurationFile:challengePassword)
 * @var type
 * 
 * <section>:<name>[:<alternateMethod>]
 */
trait TObjectArray {


    private function setPropertyValueType($reflectionProperty, $propertyValue) {
        if (($type = ComponentReflection::getPropertyType($reflectionProperty, 'string'))) {
            settype($propertyValue, $type);
        }
        return $propertyValue;
    }

    /**
     * 
     * @param string|null $name
     * @return array
     */
    public function toArray($name = NULL) {
        $return = array();
        $rproprerties = (new \ReflectionClass($this))->getProperties();
        foreach ($rproprerties as $rp) {
            if ((($toArray = ComponentReflection::parseAnnotation($rp, 'toArray')) !== NULL)) {
                $m = array();
                foreach ($toArray as $temp) {
                    if ((preg_match('#' . $name . ':([a-z0-9_-]*)(:([a-z0-9_-]*))?#iu', $temp, $m)) && (($value = $this->__get((isset($m[3])) ? $m[3] : $rp->name)) !== NULL)) {
                        $return[$m[1]] = $this->setPropertyValueType($rp, $value);
                        break;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * 
     * @param string|null $name
     * @return array
     */
    public function toArrayMethods($name = NULL) {
        $return = array();
        $rmethods = (new \ReflectionClass($this))->getMethods(\ReflectionMethod::IS_PUBLIC);
        foreach ($rmethods as $rm) {
            $mmethod = array();
            $mannotation = array();
            if ((($toArray = ComponentReflection::parseAnnotation($rm, 'toArray')) !== NULL) && (preg_match('/^(get|is)([0-9a-z]*)$/iu', $rm->name, $mmethod)) && (preg_match('#' . $name . ':([a-z0-9_-]*)#iu', $toArray, $mannotation)) && (($methodName = $mmethod[0]) !== NULL) && (($value = $this->$methodName()) !== NULL)) {
                $return[$mannotation[1]] = $this->setPropertyValueType($rm, $value);
            }
        }
        return $return;
    }

    /**
     * 
     * @param array $array
     * @param string $name
     * @return $this
     */
    public function fromArray($array = array(), $name = NULL) {
        $rproprerties = (new \ReflectionClass($this))->getProperties();
        foreach ($rproprerties as $rp) {
            if ((($fromArray = ComponentReflection::parseAnnotation($rp, 'fromArray')) !== NULL)) {
                $m = array();
                foreach ($fromArray as $temp) {
                    if ((preg_match('#' . $name . ':([a-z0-9_-]*)(:([a-z0-9_-]*))?#iu', $temp, $m)) && (isset($array[$m[1]]))) {
                        $value = $this->setPropertyValueType($rp, $array[$m[1]]);
                        $this->__set(((isset($m[3])) ? $m[3] : $rp->name), $value);
                        break;
                    }
                }
            }
        }
        return $this;
    }

    /**
     * 
     * @param array $array
     * @param string $name
     * @return $this
     */
    public function fromArrayMethods($array = array(), $name = NULL) {
        $rmethods = (new \ReflectionClass($this))->getMethods(\ReflectionMethod::IS_PUBLIC);
        foreach ($rmethods as $rm) {
            $mmethod = array();
            $mannotation = array();
            if ((($fromArray = ComponentReflection::parseAnnotation($rm, 'fromArray')) !== NULL) && (preg_match('/^(get|is)([0-9a-z]*)$/iu', $rm->name, $mmethod)) && (preg_match('#' . $name . ':([a-z0-9_-]*)#iu', $fromArray, $mannotation)) && (($methodName = $mmethod[0]) !== NULL) && (isset($array[$mannotation[1]]))) {
                $value = $this->setPropertyValueType($rm, $array[$mannotation[1]]);
                $this->$methodName($value);
            }
        }
        return $this;
    }

    /**
     * 
     * @param array $array
     * @param string $name
     * @return \static
     */
    public static function newFromArray($array = array(), $name = NULL) {
        $_this = new static();
        $rproprerties = (new \ReflectionClass($_this))->getProperties();
        foreach ($rproprerties as $rp) {
            $m = array();
            if ((($fromArray = $rp->getDocComment()) !== NULL) && (preg_match('#' . $name . ':([a-z0-9_-]*)(:([a-z0-9_-]*))?#iu', $fromArray, $m)) && (isset($array[$m[1]]))) {
                $value = $_this->setPropertyValueType($rp, $array[$m[1]]);
                $_this->__set(((isset($m[3])) ? $m[3] : $rp->name), $value);
            }
        }
        return $_this;
    }

    /**
     * 
     * @param array $array
     * @param string $name
     * @return \static
     */
    public static function newFromArrayMethods($array = array(), $name = NULL) {
        $_this = new static();
        $rmethods = (new \ReflectionClass($_this))->getMethods(\ReflectionMethod::IS_PUBLIC);
        foreach ($rmethods as $rm) {
            $mmethod = array();
            $mannotation = array();
            if ((($fromArray = ComponentReflection::parseAnnotation($rm, 'fromArray')) !== NULL) && (preg_match('/^(get|is)([0-9a-z]*)$/iu', $rm->name, $mmethod)) && (preg_match('#' . $name . ':([a-z0-9_-]*)#iu', $fromArray, $mannotation)) && (($methodName = $mmethod[0]) !== NULL) && (isset($array[$mannotation[1]]))) {
                $value = $this->setPropertyValueType($rm, $array[$mannotation[1]]);
                $_this->$methodName($value);
            }
        }
        return $_this;
    }

}
