<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

/**
 *
 * @author karel.novak
 * @property mixed $id Object Index
 */
trait TObjectId {

    protected $id = NULL;

    /**
     * Return Object Id
     * @return mixed
     */
    public function getId() {
        if (isset($this->id)) {
            return $this->id;
        }
        return spl_object_hash($this);
    }

    /**
     * Return true if Object id has been set !
     * @return bool
     */
    public function hasId() {
        return isset($this->id);
    }

    /**
     * Object Id must be scalar !
     * @param mixed
     * @throws \NetteAddons\ObjectIdException
     */
    public function setId($id) {
        if (is_scalar($id)) {
            $this->id = $id;
        } else {
            throw new \NetteAddons\ObjectIdException();
        }
        return $this;
    }

    /**
     * 
     * @param type $id
     * @return \static
     */
    public static function newObjectId($id) {
        $return = new static;
        $return->setId($id);
        return $return;
    }

}
