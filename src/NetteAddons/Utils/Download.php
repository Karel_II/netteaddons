<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

use NetteAddons\Http\Url,
    Nette,
    Nette\Utils\FileSystem;

/**
 * Description of Download
 *
 * @author Karel
 */
class Download {

    /**
     * 
     * @param array $sources
     * @param \SplFileInfo $destinationDirectory
     * @param bool $overwrite
     * @param Url $referer
     * @return array
     */
    public static function downloadFiles($sources, \SplFileInfo $destinationDirectory, $overwrite = TRUE, Url $referer = NULL) {
        $downloaded = array();
        if (is_array($sources)) {
            foreach ($sources as $source) {
                $downloaded[] = static::downloadFile($source, $destinationDirectory, $overwrite, $referer);
            }
        }
        return $downloaded;
    }

    /**
     * 
     * @param Url $source
     * @param \SplFileInfo $destinationDirectory
     * @param bool $overwrite
     * @param Url $referer
     * @return \SplFileInfo
     * @throws Nette\IOException
     */
    public static function downloadFile(Url $source, \SplFileInfo $destinationDirectory, $overwrite = TRUE, Url $referer = NULL) {
        $destination = $destinationDirectory . '/' . $source->fileName;
        if (!$overwrite && is_file($destination)) {
            return new \SplFileInfo($destination);
        }

        FileSystem::createDir($destinationDirectory);
        $httpOpts = array('http' =>
            array(
                'method' => 'GET'
            )
        );

        if (isset($referer)) {
            $httpOpts['http']['header'] = "Referer: " . $referer . "\r\n";
        }

        $httpContext = stream_context_create($httpOpts);
        if (@stream_copy_to_stream(fopen($source, 'r', FALSE, $httpContext), fopen($destination, 'w')) === FALSE) {
            throw new Nette\IOException("Unable to download file '$source' to '$destination'.");
        }

        return new \SplFileInfo($destination);
    }

}
