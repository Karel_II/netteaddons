<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

/**
 *
 * @author Karel
 */
interface IObjectDisable {

    const DISABLE_IDENTITY_ID = 0;

    /**
     * Enable
     * @return $this
     */
    public function enable();

    /**
     * Disable 
     * @return $this
     */
    public function disable();

    /**
     * 
     * @return bool|int|null
     */
    public function getDisable();

    /**
     * 
     * @return bool|\Nette\Security\Identity
     */
    public function getDisableIdentity();

    /**
     * 
     * @param null|bool|int|\Nette\Security\Identity $disable
     * @return $this
     */
    public function setDisable($disable = false);

    /**
     * 
     * @return bool
     */
    public function isDisabled();

    /**
     * 
     * @return bool
     */
    public function isEnabled();
}
