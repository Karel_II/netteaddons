<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

/**
 *
 * @author karel.novak
 */
interface IObjectArray {

    /**
     * 
     * @param string|null $name
     * @return array
     */
    public function toArray($name = NULL);

    /**
     * 
     * @param string|null $name
     * @return array
     */
    public function toArrayMethods($name = NULL);

    /**
     * 
     * @param array $array
     * @param string $name
     * @return $this
     */
    public function fromArray($array = array(), $name = NULL);

    /**
     * 
     * @param array $array
     * @param string $name
     * @return $this
     */
    public function fromArrayMethods($array = array(), $name = NULL);

    /**
     * 
     * @param array $array
     * @param string $name
     * @return \static
     */
    public static function newFromArray($array = array(), $name = NULL);

    /**
     * 
     * @param array $array
     * @param string $name
     * @return \static
     */
    public static function newFromArrayMethods($array = array(), $name = NULL);
}
