<?php

namespace NetteAddons\Utils;

use Nette;
use Nette\Utils\Callback;
use Nette\Utils\UnknownImageFileException;
use Nette\Utils\ImageException;

/**
 * Description of Image
 *
 * @author Karel_II
 */
class Image extends \Nette\Utils\Image {

    /** @int image types {@link send()} */
    const JPEG = IMAGETYPE_JPEG,
            PNG = IMAGETYPE_PNG,
            GIF = IMAGETYPE_GIF,
            BMP = IMAGETYPE_BMP;

    /**
     * Opens image from file.
     * @param  string
     * @param  mixed  detected image format
     * @throws Nette\NotSupportedException if gd extension is not loaded
     * @throws UnknownImageFileException if file not found or file type is not known
     * @return self
     */
    public static function fromFile($file, & $format = NULL) {
        if (!extension_loaded('gd')) {
            throw new Nette\NotSupportedException('PHP extension GD is not loaded.');
        }

        static $funcs = array(
            self::JPEG => 'imagecreatefromjpeg',
            self::PNG => 'imagecreatefrompng',
            self::GIF => 'imagecreatefromgif',
            self::BMP => array('\NetteAddons\Utils\Image', 'imagecreatefrombmp'),
        );
        $info = @getimagesize($file); // @ - files smaller than 12 bytes causes read error
        $format = $info[2];

        if (!isset($funcs[$format])) {
            throw new UnknownImageFileException(is_file($file) ? "Unknown type of file '$file'." : "File '$file' not found.");
        }
        return new static(Callback::invokeSafe($funcs[$format], array((string) $file), function ($message) {
                    throw new ImageException($message);
                }));
    }

    /**
     * @deprecated
     */
    public static function getFormatFromString($s) {
        trigger_error(__METHOD__ . '() is deprecated; use finfo_buffer() instead.', E_USER_DEPRECATED);
        $types = array('image/jpeg' => self::JPEG, 'image/gif' => self::GIF, 'image/png' => self::PNG, 'image/x-ms-bmp' => self::BMP);
        $type = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $s);
        return isset($types[$type]) ? $types[$type] : NULL;
    }

    /**
     * Create a new image from the image stream in the string.
     * @param  string
     * @param  mixed  detected image format
     * @return self
     * @throws ImageException
     */
    public static function fromString($s, & $format = NULL) {
        if (!extension_loaded('gd')) {
            throw new Nette\NotSupportedException('PHP extension GD is not loaded.');
        }

        if (func_num_args() >= 1) {
            $format = @static::getFormatFromString($s); // @ suppress trigger_error
        }

        if ($format == self::BMP) {
            return new static(Callback::invokeSafe(array('\NetteAddons\Utils\Image', 'imagecreatefrombmpstring'), array($s), function ($message) {
                        throw new ImageException($message);
                    }));
        }

        return new static(Callback::invokeSafe('imagecreatefromstring', array($s), function ($message) {
                    throw new ImageException($message);
                }));
    }

    private static function imagefrombmpresource($handle) {
        //1 : Chargement des ent?tes FICHIER
        $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($handle, 14));
        if ($FILE['file_type'] != 19778) {
            return FALSE;
        }

        //2 : Chargement des ent?tes BMP
        $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel' .
                '/Vcompression/Vsize_bitmap/Vhoriz_resolution' .
                '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($handle, 40));
        $BMP['colors'] = pow(2, $BMP['bits_per_pixel']);
        if ($BMP['size_bitmap'] == 0) {
            $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
        }
        $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel'] / 8;
        $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
        $BMP['decal'] = ($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
        $BMP['decal'] -= floor($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
        $BMP['decal'] = 4 - (4 * $BMP['decal']);
        if ($BMP['decal'] == 4) {
            $BMP['decal'] = 0;
        }
        //3 : Chargement des couleurs de la palette
        $PALETTE = array();
        if ($BMP['colors'] < 16777216) {
            $PALETTE = unpack('V' . $BMP['colors'], fread($handle, $BMP['colors'] * 4));
        }

        //4 : Cr?ation de l'image
        $IMG = fread($handle, $BMP['size_bitmap']);
        $VIDE = chr(0);

        $res = imagecreatetruecolor($BMP['width'], $BMP['height']);
        $P = 0;
        $Y = $BMP['height'] - 1;
        while ($Y >= 0) {
            $X = 0;
            while ($X < $BMP['width']) {
                if ($BMP['bits_per_pixel'] == 24) {
                    $COLOR = unpack("V", substr($IMG, $P, 3) . $VIDE);
                } elseif ($BMP['bits_per_pixel'] === 16) {
                    $COLOR = unpack("n", substr($IMG, $P, 2));
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] === 8) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, $P, 1));
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] === 4) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));
                    if (($P * 2) % 2 == 0) {
                        $COLOR[1] = ($COLOR[1] >> 4);
                    } else {
                        $COLOR[1] = ($COLOR[1] & 0x0F);
                    }
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] === 1) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));
                    if (($P * 8) % 8 == 0) {
                        $COLOR[1] = $COLOR[1] >> 7;
                    } elseif (($P * 8) % 8 === 1) {
                        $COLOR[1] = ($COLOR[1] & 0x40) >> 6;
                    } elseif (($P * 8) % 8 === 2) {
                        $COLOR[1] = ($COLOR[1] & 0x20) >> 5;
                    } elseif (($P * 8) % 8 === 3) {
                        $COLOR[1] = ($COLOR[1] & 0x10) >> 4;
                    } elseif (($P * 8) % 8 === 4) {
                        $COLOR[1] = ($COLOR[1] & 0x8) >> 3;
                    } elseif (($P * 8) % 8 === 5) {
                        $COLOR[1] = ($COLOR[1] & 0x4) >> 2;
                    } elseif (($P * 8) % 8 === 6) {
                        $COLOR[1] = ($COLOR[1] & 0x2) >> 1;
                    } elseif (($P * 8) % 8 === 7) {
                        $COLOR[1] = ($COLOR[1] & 0x1);
                    }
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } else {
                    return FALSE;
                }
                imagesetpixel($res, $X, $Y, $COLOR[1]);
                $X++;
                $P += $BMP['bytes_per_pixel'];
            }
            $Y--;
            $P += $BMP['decal'];
        }

        //Fermeture du fichier
        fclose($handle);

        return $res;
    }

    public static function imagecreatefrombmp($file) {

        if (!$handle = fopen($file, 'rb')) {
            return FALSE;
        }
        return self::imagefrombmpresource($handle);
    }

    public static function imagecreatefrombmpstring($string) {
        if (!$handle = fopen('php://memory', 'r+')) {
            return FALSE;
        }
        fwrite($handle, $string);
        rewind($handle);
        return self::imagefrombmpresource($handle);
    }

}
