<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Utils;

use Nette,
    Nette\Utils\ArrayHash;

/**
 * Description of ArrayHashObject
 *
 * @author karel.novak
 */
abstract class ArrayHashObject implements \ArrayAccess, \Countable, \IteratorAggregate {

    /**
     *
     * @var Nette\Utils\ArrayHash 
     */
    protected $arrayHash = null;

    /**
     *
     * @var string 
     */
    protected $arrayHashValuesClass = '\NetteAddons\Utils\IObjectId';

    public function __construct() {
        $this->arrayHash = new ArrayHash();
    }

    /**
     * 
     * @param App\Model\Link\ArrayHashBase $arrayHashBase
     * @throws Nette\InvalidArgumentException
     */
    public function merge($arrayHashBase) {
        if (get_class($this) === get_class($arrayHashBase)) {
            foreach ($arrayHashBase as $key => $value) {
                $this->arrayHash->$key = $value;
            }
        } else {
            throw new Nette\InvalidArgumentException(sprintf('Value must be either a %s, %s given.', get_class($this), get_class($arrayHashBase)));
        }
    }

    /**
     * Returns an iterator over all items.
     * @return \RecursiveArrayIterator
     */
    public function getIterator() {
        return new \RecursiveArrayIterator($this->arrayHash);
    }

    /**
     * Returns items count.
     * @return int
     */
    public function count() {
        return count((array) $this->arrayHash);
    }

    /**
     * Replaces or appends a item.
     * @return void
     */
    public function offsetSet($key, $value) {

        if ((!isset($key) || !is_scalar($key)) && !is_scalar($value->getId())) {
            throw new Nette\InvalidArgumentException(sprintf('Key or Value Id must be either a string or an integer, %s or %s given.', gettype($value->getId()), gettype($key)));
        } elseif (isset($key) && !$value->hasId()) {
            $value->setId($key);
        } elseif (!isset($key)) {
            $key = $value->getId();
        }
        if (!is_a($value, $this->arrayHashValuesClass)) {
            throw new Nette\InvalidArgumentException(sprintf('Value must be either a ' . $this->arrayHashValuesClass . ', %s given.', get_class($value)));
        }
        $this->arrayHash->$key = $value;
    }

    /**
     * Returns a item.
     * @return mixed
     */
    public function offsetGet($key) {
        return $this->arrayHash->$key;
    }

    /**
     * Determines whether a item exists.
     * @return bool
     */
    public function offsetExists($key) {
        return isset($this->arrayHash->$key);
    }

    /**
     * Removes the element from this list.
     * @return void
     */
    public function offsetUnset($key) {
        unset($this->arrayHash->$key);
    }

    /**
     * Removes the element from this list and destroy the element
     * @param type $key
     */
    public function valueUnset($key) {
        $value = $this->arrayHash->$key;
        unset($this->arrayHash->$key);
        unset($value);
    }

    /**
     * Return array of keys
     * @return Array
     */
    public function getKeys() {
        return array_keys((array) $this->arrayHash);
    }

    /**
     * Return key
     * @return int
     */
    public function getKey() {
        return key($this->arrayHash);
    }

    public function slice($offset, $length = NULL, $preserve_keys = false) {
        $slice = new static;

        if ($this->count() == 0) {
            return $slice;
        }
        for ($iterator = $this->getIterator(), $iterator->seek($offset), $i = $offset; $iterator->valid() && $i < ($offset + $length); $iterator->next(), $i++) {
            if ($preserve_keys) {
                $key = $iterator->key();
            } else {
                $key = (isset($key)) ? $key + 1 : 0;
            }
            $value = $iterator->current();
            $slice[$key] = $value;
        }
        return $slice;
    }

    public function natsort() {
        $this->getIterator()->natsort();
    }

    /**
     * Return array of strings representing objects.
     * @return type
     */
    public function getStringsArray() {
        $return = array();
        foreach ($this->arrayHash as $key => $value) {
            $return[$key] = (string) $value;
        }
        return $return;
    }

}
