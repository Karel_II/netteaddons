<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\DI;

use NetteAddons\Application\UI\SetupFormFactory;

/**
 * Description of SetupFormsExtension
 *
 * @author karel.novak
 */
class SetupFormsExtension extends \Nette\DI\CompilerExtension {

    private $defaults = [
        'formRenderer' => NULL,
    ];

    /**
     * 
     * @return void
     */
    public function loadConfiguration() {
        $this->validateConfig($this->defaults);

        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('setupForms'))
                ->setFactory(SetupFormFactory::class)
                ->addSetup('setRenderer', [$this->config['formRenderer']]);
    }

}
