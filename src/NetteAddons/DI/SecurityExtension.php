<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\DI;

use \Nette\Security\Passwords;
use \NetteAddons\Repository\UsersRepository,
    \NetteAddons\Repository\IdentityRolesRepository,
    \NetteAddons\Repository\ResourcesRepository,
    \NetteAddons\Repository\RolesRepository,
    \NetteAddons\Repository\VerifyRepository,
    \NetteAddons\Repository\VisitorsRepository,
    \NetteAddons\Repository\Create,
    \Netteaddons\Security\UserManager;
use \NetteAddons\Application\UI\IChangePasswordFormFactory,
    \NetteAddons\Application\UI\IChangeUserFormFactory,
    \NetteAddons\Application\UI\IChangeRolesFormFactory,
    \NetteAddons\Application\UI\IForgotPasswordFormFactory,
    \NetteAddons\Application\UI\ISignUpFormFactory,
    \NetteAddons\Application\UI\ISignInFormFactory;

/**
 * Description of SecurityExtension
 *
 * @author karel.novak
 */
class SecurityExtension extends \Nette\DI\CompilerExtension {

    private $defaults = [
    ];

    /**
     * 
     * @return void
     */
    public function loadConfiguration() {
        $this->validateConfig($this->defaults);

        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('passwords'))
                ->setFactory(Passwords::class);

        $builder->addDefinition($this->prefix('createResourcesRepository'))
                ->setFactory(Create\ResourcesRepository::class);
        $builder->addDefinition($this->prefix('createRolesRepository'))
                ->setFactory(Create\RolesRepository::class);
        $builder->addDefinition($this->prefix('createUsersRepository'))
                ->setFactory(Create\UsersRepository::class);
        $builder->addDefinition($this->prefix('createVisitorsRepository'))
                ->setFactory(Create\VisitorsRepository::class);

        $builder->addDefinition($this->prefix('resourcesRepository'))
                ->setFactory(ResourcesRepository::class)
                ->addSetup('setRepositoryCreate', [$this->prefix('@createResourcesRepository')]);
        $builder->addDefinition($this->prefix('rolesRepository'))
                ->setFactory(RolesRepository::class)
                ->addSetup('setRepositoryCreate', [$this->prefix('@createRolesRepository')]);
        $builder->addDefinition($this->prefix('usersRepository'))
                ->setFactory(UsersRepository::class)
                ->addSetup('setRepositoryCreate', [$this->prefix('@createUsersRepository')]);
        $builder->addDefinition($this->prefix('IdentityRolesRepository'))
                ->setFactory(IdentityRolesRepository::class);
        $builder->addDefinition($this->prefix('visitorsRepository'))
                ->setFactory(VisitorsRepository::class)
                ->addSetup('setRepositoryCreate', [$this->prefix('@createVisitorsRepository')]);
        $builder->addDefinition($this->prefix('verifyRepository'))
                ->setFactory(VerifyRepository::class)
                ->addSetup('setPasswords', [$this->prefix('@passwords')]);

        $builder->addDefinition($this->prefix('userManager'))
                ->setFactory(UserManager::class);

        $builder->addFactoryDefinition($this->prefix('ichangePasswordFormFactory'))
                ->setImplement(IChangePasswordFormFactory::class);
        $builder->addFactoryDefinition($this->prefix('ichangeUserFormFactory'))
                ->setImplement(IChangeUserFormFactory::class);
        $builder->addFactoryDefinition($this->prefix('ichangeRolesFormFactory'))
                ->setImplement(IChangeRolesFormFactory::class);
        $builder->addFactoryDefinition($this->prefix('iforgotPasswordFormFactory'))
                ->setImplement(IForgotPasswordFormFactory::class);
        $builder->addFactoryDefinition($this->prefix('isignUpFormFactory'))
                ->setImplement(ISignUpFormFactory::class);
        $builder->addFactoryDefinition($this->prefix('isignInFormFactory'))
                ->setImplement(ISignInFormFactory::class);
    }
}
