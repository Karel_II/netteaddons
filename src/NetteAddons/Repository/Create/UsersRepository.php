<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of UsersRepository
 *
 * @author karel.novak
 */
class UsersRepository extends Repository\Repository implements Repository\IRepositoryCreate{

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {
        $this->context->beginTransaction();

        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\UsersRepository::TABLE_USER_NAME . "` ( " .
                "`" . Repository\UsersRepository::COLUMN_USER_ID . "`                 INTEGER " . $this->getUnsigned() . "NOT NULL " . $this->getColumnPrimaryKey(true) . ", " .
                "`" . Repository\UsersRepository::COLUMN_USER_NAME . "`               CHAR (50) NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_FIRST_NAME . "`              VARCHAR (30) DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_MIDDLE_NAME . "`             VARCHAR (30) DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_LAST_NAME . "`               VARCHAR (30) DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_EMAIL . "`              VARCHAR (50) DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_PASSWORD_HASH . "`      CHAR (60) NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_KEY_OTP . "`            CHAR (160) DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_KEY_API . "`            CHAR (160) DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_EXPIRE_ACCOUNT . "`     DATETIME DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_EXIPIRE_PASSWORD . "`   DATETIME DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_PROFILE_IMAGE . "`      BLOB DEFAULT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT . "`  INTEGER " . $this->getUnsigned() . "NOT NULL DEFAULT '0', " .
                "`" . Repository\UsersRepository::COLUMN_USER_DISABLED . "`           TINYINT " . $this->getUnsigned() . "NOT NULL DEFAULT '0', " .
                "`" . Repository\UsersRepository::COLUMN_USER_DELETED . "`            TINYINT " . $this->getUnsigned() . "NOT NULL DEFAULT '0', " .
                (($result = $this->addKey('PRIMARY_AUTO_INCREMENT', Repository\UsersRepository::COLUMN_USER_ID, [Repository\UsersRepository::COLUMN_USER_ID])) . ($result ? " ," : '')) .
                (($result = $this->addKey('UNIQUE', Repository\UsersRepository::COLUMN_USER_NAME, [Repository\UsersRepository::COLUMN_USER_NAME])) . ($result ? " ," : '')) .
                (($result = $this->addKey('UNIQUE', Repository\UsersRepository::COLUMN_USER_EMAIL, [Repository\UsersRepository::COLUMN_USER_EMAIL]))) .
                ");"
        );

        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\UsersRepository::TABLE_USERROLES_NAME . "` ( " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_USERID . "`        INTEGER " . $this->getUnsigned() . "NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_ROLE . "`          CHAR (20) NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_ORDER . "`         TINYINT NOT NULL DEFAULT '0', " .
                (($result = $this->addKey('PRIMARY', Repository\UsersRepository::COLUMN_USERROLES_USERID, [Repository\UsersRepository::COLUMN_USERROLES_USERID, Repository\UsersRepository::COLUMN_USERROLES_ROLE])) . ($result ? " ," : '')) .
                "FOREIGN KEY(`" . Repository\UsersRepository::COLUMN_USERROLES_USERID . "`) REFERENCES `" . Repository\UsersRepository::TABLE_USER_NAME . "`(`" . Repository\UsersRepository::COLUMN_USER_ID . "`), " .
                "FOREIGN KEY(`" . Repository\UsersRepository::COLUMN_USERROLES_ROLE . "`)   REFERENCES `" . Repository\RolesRepository::TABLE_ROLES_NAME . "`(`" . Repository\RolesRepository::COLUMN_ROLES_NAME . "`) " .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\UsersRepository::TABLE_USERDYNAMIC_NAME . "` ( " .
                "`" . Repository\UsersRepository::COLUMN_USERDYNAMIC_USERID . "`        INTEGER " . $this->getUnsigned() . "NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USERDYNAMIC_KEY_OTP_PASSWORD_RECOVERY . "`            CHAR (160) DEFAULT NULL, " .
                (($result = $this->addKey('PRIMARY', Repository\UsersRepository::COLUMN_USERDYNAMIC_USERID, [Repository\UsersRepository::COLUMN_USERDYNAMIC_USERID])) . ($result ? " ," : '')) .
                "FOREIGN KEY(`" . Repository\UsersRepository::COLUMN_USERDYNAMIC_USERID . "`) REFERENCES `" . Repository\UsersRepository::TABLE_USER_NAME . "`(`" . Repository\UsersRepository::COLUMN_USER_ID . "`) " .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );
        
        
        //$this->alterAddColumn(Repository\UsersRepository::TABLE_USER_NAME, "imagePerPage", "tinyint ( 3 ) NOT NULL DEFAULT '20'");
        $this->context->commit();
    }

}
