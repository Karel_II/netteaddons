<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of ResourcesRepository
 *
 * @author Karel
 */
class ResourcesRepository extends Repository\Repository implements Repository\IRepositoryCreate {

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {
        $this->context->beginTransaction();
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\ResourcesRepository::TABLE_RESOURCES_NAME . "` ( " .
                "`" . Repository\ResourcesRepository::COLUMN_RESOURCES_NAME . "`         CHAR (20) NOT NULL, " .
                $this->addKey('PRIMARY', Repository\ResourcesRepository::COLUMN_RESOURCES_NAME, [Repository\ResourcesRepository::COLUMN_RESOURCES_NAME]) . "" .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\ResourcesRepository::TABLE_RESOURCESPARENTS_NAME . "` ( " .
                "`" . Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE . "`     CHAR (20) NOT NULL, " .
                "`" . Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_PARENT . "`       CHAR (20) NOT NULL, " .
                "`" . Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_ORDER . "`        TINYINT NOT NULL DEFAULT '0', " .
                (($result = $this->addKey('PRIMARY', Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE, [Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE, Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_PARENT])) . ($result ? " ," : '')) .
                "FOREIGN KEY(`" . Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE . "`)   REFERENCES `" . Repository\ResourcesRepository::TABLE_RESOURCES_NAME . "`(`" . Repository\ResourcesRepository::COLUMN_RESOURCES_NAME . "`), " .
                "FOREIGN KEY(`" . Repository\ResourcesRepository::COLUMN_RESOURCESPARENTS_PARENT . "`) REFERENCES `" . Repository\ResourcesRepository::TABLE_RESOURCES_NAME . "`(`" . Repository\ResourcesRepository::COLUMN_RESOURCES_NAME . "`) " .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );

        $this->insertIgnore([
            array(Repository\ResourcesRepository::COLUMN_RESOURCES_NAME => 'administration')
                ], Repository\ResourcesRepository::TABLE_RESOURCES_NAME);
        $this->context->commit();
    }

}
