<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of VisitorsRepository
 *
 * @author Karel
 */
class VisitorsRepository extends Repository\Repository implements Repository\IRepositoryCreate {

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {
        $this->context->beginTransaction();
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\VisitorsRepository::TABLE_VISITORS_NAME . "` ( " .
                "`" . Repository\VisitorsRepository::COLUMN_VISITORS_USERID . "`     INTEGER " . $this->getUnsigned() . "DEFAULT NULL, " .
                "`" . Repository\VisitorsRepository::COLUMN_VISITORS_USERID_NN . "`  INTEGER " . $this->getUnsigned() . "NOT NULL DEFAULT '0', " .
                "`" . Repository\VisitorsRepository::COLUMN_VISITORS_IP . "`         CHAR(20) NOT NULL, " .
                "`" . Repository\VisitorsRepository::COLUMN_VISITORS_DATETIME . "`   DATETIME NOT NULL, " .
                "`" . Repository\VisitorsRepository::COLUMN_VISITORS_DATE . "`       DATE NOT NULL, " .
                (($result = $this->addKey('PRIMARY', Repository\VisitorsRepository::COLUMN_VISITORS_USERID_NN, [Repository\VisitorsRepository::COLUMN_VISITORS_USERID_NN, Repository\VisitorsRepository::COLUMN_VISITORS_IP, Repository\VisitorsRepository::COLUMN_VISITORS_DATE])) . ($result ? " ," : '')) .
                "FOREIGN KEY(`" . Repository\VisitorsRepository::COLUMN_VISITORS_USERID . "`) REFERENCES `" . Repository\UsersRepository::TABLE_USER_NAME . "`(`" . Repository\UsersRepository::COLUMN_USER_ID . "`) " .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );
        $this->context->commit();
    }

}
