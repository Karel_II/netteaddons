<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of RolesRepository
 *
 * @author karel.novak
 */
class RolesRepository extends Repository\Repository implements Repository\IRepositoryCreate{

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {
        $this->context->beginTransaction();
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\RolesRepository::TABLE_ROLES_NAME . "` ( " .
                "`" . Repository\RolesRepository::COLUMN_ROLES_NAME . "`         CHAR (20) NOT NULL, " .
                $this->addKey('PRIMARY', Repository\RolesRepository::COLUMN_ROLES_NAME, [Repository\RolesRepository::COLUMN_ROLES_NAME]) . "" .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\RolesRepository::TABLE_ROLESPARENTS_NAME . "` ( " .
                "`" . Repository\RolesRepository::COLUMN_ROLESPARENTS_ROLE . "`     CHAR (20) NOT NULL, " .
                "`" . Repository\RolesRepository::COLUMN_ROLESPARENTS_PARENT . "`   CHAR (20) NOT NULL, " .
                "`" . Repository\RolesRepository::COLUMN_ROLESPARENTS_ORDER . "`    TINYINT NOT NULL DEFAULT '0', " .
                (($result = $this->addKey('PRIMARY', Repository\RolesRepository::COLUMN_ROLESPARENTS_ROLE, [Repository\RolesRepository::COLUMN_ROLESPARENTS_ROLE, Repository\RolesRepository::COLUMN_ROLESPARENTS_PARENT])) . ($result ? " ," : '')) .
                "FOREIGN KEY(`" . Repository\RolesRepository::COLUMN_ROLESPARENTS_ROLE . "`)   REFERENCES `" . Repository\RolesRepository::TABLE_ROLES_NAME . "`(`" . Repository\RolesRepository::COLUMN_ROLES_NAME . "`), " .
                "FOREIGN KEY(`" . Repository\RolesRepository::COLUMN_ROLESPARENTS_PARENT . "`) REFERENCES `" . Repository\RolesRepository::TABLE_ROLES_NAME . "`(`" . Repository\RolesRepository::COLUMN_ROLES_NAME . "`) " .
                ")" . (($this->storageEngine !== NULL) ? " ENGINE = $this->storageEngine" : "") . ";"
        );

        $this->insertIgnore([
            array(Repository\RolesRepository::COLUMN_ROLES_NAME => 'guest'), // default role for unauthenticated user
            array(Repository\RolesRepository::COLUMN_ROLES_NAME => 'authenticated'), // default role for authenticated user without own identity
            array(Repository\RolesRepository::COLUMN_ROLES_NAME => 'user'),
            array(Repository\RolesRepository::COLUMN_ROLES_NAME => 'admin'),
            array(Repository\RolesRepository::COLUMN_ROLES_NAME => 'root'),
                ], Repository\RolesRepository::TABLE_ROLES_NAME);

        $this->insertIgnore([
            array(
                Repository\RolesRepository::COLUMN_ROLESPARENTS_ROLE => 'admin',
                Repository\RolesRepository::COLUMN_ROLESPARENTS_PARENT => 'user',
                Repository\RolesRepository::COLUMN_ROLESPARENTS_ORDER => 0
            ),
            array(
                Repository\RolesRepository::COLUMN_ROLESPARENTS_ROLE => 'root',
                Repository\RolesRepository::COLUMN_ROLESPARENTS_PARENT => 'admin',
                Repository\RolesRepository::COLUMN_ROLESPARENTS_ORDER => 1
            )
                ], Repository\RolesRepository::TABLE_ROLESPARENTS_NAME);
        $this->context->commit();
    }

}
