<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use \Nette\Database\Context,
    \Nette\Security\User;

/**
 * Description of RolesRepository
 *
 * @author karel.novak
 */
class RolesRepository extends Repository {

    use TRepositorySecurity;

    const
            TABLE_ROLES_NAME = '_securityRoles',
            COLUMN_ROLES_NAME = 'name';
    const
            TABLE_ROLESPARENTS_NAME = '_securityRolesParents',
            COLUMN_ROLESPARENTS_ROLE = 'role',
            COLUMN_ROLESPARENTS_PARENT = 'parent',
            COLUMN_ROLESPARENTS_ORDER = 'orders';
    const
            RESOURCE_MANAGEMENT_ROLE = 'resourceManagementRole';

    public function __construct(Context $context, User $securityUser) {
        parent::__construct($context);
        $this->setSecurityUser($securityUser);
    }

    public function getRoles() {
        $roles = array();
        $result = $this->getTable(self::TABLE_ROLES_NAME)->select(RolesRepository::COLUMN_ROLES_NAME);
        foreach ($result as $row) {
            if ($this->isAllowed(RolesRepository::RESOURCE_MANAGEMENT_ROLE, $row[RolesRepository::COLUMN_ROLES_NAME])) {
                $roles[] = $row[RolesRepository::COLUMN_ROLES_NAME];
            }
        }
        return $roles;
    }

}
