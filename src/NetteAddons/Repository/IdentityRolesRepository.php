<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

/**
 * Description of IdentityRolesRepository
 *
 * @author Karel
 */
class IdentityRolesRepository {

    use TRepositorySecurity;

    const
            TABLE_IDENTITYROLES_NAME = '_securityUsersRoles',
            COLUMN_IDENTITYROLES_USERID = 'userId',
            COLUMN_IDENTITYROLES_ROLE = 'role',
            COLUMN_IDENTITYROLES_ORDER = 'orders';

    /**
     * 
     * @param Identity $identity
     * @param string $role
     * @param integer $order
     * @return boolean
     */
    public function idtentityRoleAdd(Identity $identity, $role, $order = NULL) {
        if (!($this->rightsUserManageVerify($identity->id) && $this->rightsRoleVerify($role))) {
            return false;
        }
        $this->context->beginTransaction();
        $resultIdentity = $this->getTable(self::TABLE_IDENTITYROLES_NAME)->
                select(UsersRepository::COLUMN_USER_ID)->
                where(UsersRepository::COLUMN_USER_ID, $identity->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $resultMinMax = $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                    select('IFNULL(MAX(' . self::COLUMN_IDENTITYROLES_ORDER . ')+1, 0) AS `order`')->
                    select('IFNULL(MIN(' . self::COLUMN_IDENTITYROLES_ORDER . '), 0) AS `min`');
            if (($rowMinMax = $resultMinMax->fetch())) {
                $order = (is_numeric($order) && $rowMinMax['order'] >= $order) ? $order : $rowMinMax['order'];
                $resultMoveBlock = $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID);
                if ($rowMinMax['min'] > 0) {
                    $resultMoveBlock->
                            where(self::COLUMN_IDENTITYROLES_ORDER . ' <= ?', $order)->
                            update(array(self::COLUMN_IDENTITYROLES_ORDER => new SqlLiteral('`' . self::COLUMN_IDENTITYROLES_ORDER . '` - 1')));
                } else {
                    $resultMoveBlock->
                            where(self::COLUMN_IDENTITYROLES_ORDER . ' >= ?', $order)->
                            update(array(self::COLUMN_IDENTITYROLES_ORDER => new SqlLiteral('`' . self::COLUMN_IDENTITYROLES_ORDER . '` + 1')));
                }
            } else {
                $order = 0;
            }
            $insert = array();
            $insert[self::COLUMN_IDENTITYROLES_ROLE] = $role;
            $insert[self::COLUMN_IDENTITYROLES_ORDER] = $order;
            $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                    insert($insert);
        } else {
            $this->context->rollBack();
            return false;
        }
        $this->context->commit();
        return true;
    }

    /**
     * 
     * @param Identity $identity
     * @param string $role
     * @return boolean
     */
    public function idtentityRoleDelete(Identity $identity, $role) {
        if (!($this->rightsUserManageVerify($identity->id) && $this->rightsRoleVerify($role))) {
            return false;
        }
        $order = NULL;
        $this->context->beginTransaction();
        $resultIdentity = $this->getTable(self::TABLE_IDENTITYROLES_NAME)->
                select(UsersRepository::COLUMN_USER_ID)->
                where(UsersRepository::COLUMN_USER_ID, $identity->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $resultRole = $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                    select(self::COLUMN_IDENTITYROLES_ROLE)->
                    select(self::COLUMN_IDENTITYROLES_ORDER)->
                    where(self::COLUMN_IDENTITYROLES_ROLE, $role)->
                    where(self::COLUMN_IDENTITYROLES_ROLE . ' IS NOT NULL');
            if (($rowRole = $resultRole->fetch()) && isset($rowRole[self::COLUMN_IDENTITYROLES_ORDER])) {
                $order = $rowRole[self::COLUMN_IDENTITYROLES_ORDER];
                $rowRole->delete();
            } else {
                $this->context->rollBack();
                return false;
            }
            $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                    where(self::COLUMN_IDENTITYROLES_ORDER . ' >= ?', $order)->
                    update(array(self::COLUMN_IDENTITYROLES_ORDER => new SqlLiteral('`' . self::COLUMN_IDENTITYROLES_ORDER . '` - 1')));
        } else {
            $this->context->rollBack();
            return false;
        }
        $this->context->commit();
        return true;
    }

    /**
     * 
     * @param Identity $identity
     * @param string $role
     * @param integer $order
     * @return boolean
     */
    public function idtentityRoleMove(Identity $identity, $role, $order = NULL) {
        if (!($this->rightsUserManageVerify($identity->id) && $this->rightsRoleVerify($role))) {
            return false;
        }
        $this->context->beginTransaction();
        $resultIdentity = $this->getTable(self::TABLE_IDENTITYROLES_NAME)->
                select(UsersRepository::COLUMN_USER_ID)->
                where(UsersRepository::COLUMN_USER_ID, $identity->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $resultRole = $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                    select(self::COLUMN_IDENTITYROLES_ROLE)->
                    select(self::COLUMN_IDENTITYROLES_ORDER)->
                    where(self::COLUMN_IDENTITYROLES_ROLE, $role)->
                    where(self::COLUMN_IDENTITYROLES_ROLE . ' IS NOT NULL');
            if (($rowRole = $resultRole->fetch()) && isset($rowRole[self::COLUMN_IDENTITYROLES_ORDER])) {
                $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                        where(self::COLUMN_IDENTITYROLES_ORDER . ' >= ?', $rowRole[self::COLUMN_IDENTITYROLES_ORDER])->
                        where(self::COLUMN_IDENTITYROLES_ROLE . ' NOT LIKE', $role)->
                        update(array(self::COLUMN_IDENTITYROLES_ORDER => new SqlLiteral('`' . self::COLUMN_IDENTITYROLES_ORDER . '` - 1')));

                $resultMinMax = $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID)->
                        select('IFNULL(MAX(' . self::COLUMN_IDENTITYROLES_ORDER . ')+1, 0) AS `order`')->
                        select('IFNULL(MIN(' . self::COLUMN_IDENTITYROLES_ORDER . '), 0) AS `min`');
                if (($rowMinMax = $resultMinMax->fetch())) {
                    $order = (is_numeric($order) && $rowMinMax['order'] >= $order) ? $order : $rowMinMax['order'];
                    $resultMoveBlock = $rowIdentity->related(self::TABLE_IDENTITYROLES_NAME, self::COLUMN_IDENTITYROLES_USERID);
                    if ($rowMinMax['min'] > 0) {
                        $resultMoveBlock->
                                where(self::COLUMN_IDENTITYROLES_ORDER . ' <= ?', $order)->
                                update(array(self::COLUMN_IDENTITYROLES_ORDER => new SqlLiteral('`' . self::COLUMN_IDENTITYROLES_ORDER . '` - 1')));
                    } else {
                        $resultMoveBlock->
                                where(self::COLUMN_IDENTITYROLES_ORDER . ' >= ?', $order)->
                                update(array(self::COLUMN_IDENTITYROLES_ORDER => new SqlLiteral('`' . self::COLUMN_IDENTITYROLES_ORDER . '` + 1')));
                    }
                } else {
                    $order = 0;
                }
                $rowRole->update(array(self::COLUMN_IDENTITYROLES_ORDER => $order));
            } else {
                $this->context->rollBack();
                return false;
            }
        } else {
            $this->context->rollBack();
            return false;
        }
        $this->context->commit();
        return true;
    }

}
