<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use \Nette\Http\IRequest,
    \Nette\Utils\ArrayHash,
    \Nette\Utils\ArrayList,
    \Nette\Database\Context,
    \Nette\Database\SqlLiteral,
    \Nette\Security\User;
use \NetteAddons\Repository\TRepositorySecurity;

/**
 * Description of VisitorsRepository
 *
 * @author Karel
 */
class VisitorsRepository extends \NetteAddons\Repository\Repository {

    use TRepositorySecurity;

    const
            TABLE_VISITORS_NAME = '_securityVisitors',
            COLUMN_VISITORS_USERID = 'userId',
            COLUMN_VISITORS_USERID_NN = 'userIdNn',
            COLUMN_VISITORS_IP = 'IP',
            COLUMN_VISITORS_DATETIME = 'datetime',
            COLUMN_VISITORS_DATE = 'date';

    /**
     *
     * @var IRequest 
     */
    private $httpRequest = NULL;

    public function __construct(Context $context, User $securityUser, IRequest $httpRequest) {
        $this->httpRequest = $httpRequest;
        parent::__construct($context);
        $this->setSecurityUser($securityUser);
    }

    public function insertVisitor() {
        if (empty($this->httpRequest->getRemoteAddress())) {
            return;
        }
        $insert = array();
        $insert[self::COLUMN_VISITORS_USERID] = ($this->securityUser->loggedIn) ? $this->securityUser->id : NULL;
        $insert[self::COLUMN_VISITORS_USERID_NN] = ($this->securityUser->loggedIn) ? $this->securityUser->id : -1;
        $insert[self::COLUMN_VISITORS_IP] = $this->httpRequest->getRemoteAddress();
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            $insert[self::COLUMN_VISITORS_DATETIME] = new SqlLiteral('NOW()');
            $insert[self::COLUMN_VISITORS_DATE] = new SqlLiteral('NOW()');
            $this->context->query('INSERT INTO `' . self::TABLE_VISITORS_NAME . '`', $insert, 'ON DUPLICATE KEY UPDATE', $insert);
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            $insert[self::COLUMN_VISITORS_DATETIME] = new SqlLiteral('DATETIME(\'now\')');
            $insert[self::COLUMN_VISITORS_DATE] = new SqlLiteral('DATE(\'now\')');
            $this->context->query('INSERT INTO `' . self::TABLE_VISITORS_NAME . '`', $insert, 'ON CONFLICT(`' . self::COLUMN_VISITORS_USERID_NN . '`,`' . self::COLUMN_VISITORS_IP . '`,`' . self::COLUMN_VISITORS_DATE . '`) DO UPDATE SET ' . self::COLUMN_VISITORS_DATETIME . ' = excluded.' . self::COLUMN_VISITORS_DATETIME . ',  ' . self::COLUMN_VISITORS_DATE . '=excluded.' . self::COLUMN_VISITORS_DATE . ';');
        }
    }

    public function getVisitors() {
        $visitors = new ArrayHash();
        $resultTotal = $this->getTable(self::TABLE_VISITORS_NAME)->select('COUNT(*) AS `total`');
        $resultToday = $this->getTable(self::TABLE_VISITORS_NAME)->select('COUNT(*) AS `today`');
        $resultOnline = $this->getTable(self::TABLE_VISITORS_NAME)->select('COUNT(*) AS `online`');
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            $resultToday->where('DATE(`' . self::COLUMN_VISITORS_DATE . '`)=DATE(NOW())');
            $resultOnline->where('`' . self::COLUMN_VISITORS_DATETIME . '` >= (NOW() - INTERVAL 3 MINUTE)');
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            $resultToday->where('DATE(`' . self::COLUMN_VISITORS_DATETIME . '`)=DATE(\'NOW\')');
            $resultOnline->where('JULIANDAY(`' . self::COLUMN_VISITORS_DATETIME . '`) >= JULIANDAY(\'NOW\',\'-3 MINUTE\')');
        }
        if (($rowTotal = $resultTotal->fetch())) {
            $visitors->total = $rowTotal->total;
        }
        if (($rowToday = $resultToday->fetch())) {
            $visitors->today = $rowToday->today;
        }
        if (($rowOnline = $resultOnline->fetch())) {
            $visitors->online = $rowOnline->online;
        }
        return $visitors;
    }

    public function getUniqueIPAddress() {
        $return = new ArrayList();
        $result = $this->getTable(self::TABLE_VISITORS_NAME)
                ->select('DISTINCT `' . self::COLUMN_VISITORS_IP . '`');
        foreach ($result as $row) {
            $return[] = $row[self::COLUMN_VISITORS_IP];
        }
        return $return;
    }

}
