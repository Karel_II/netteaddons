<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use \Nette\Security\IAuthenticator;
use \Nette\Database\Table\IRow;
use \Nette\Database\SqlLiteral;
use \Nette\Security\Permission,
    \Nette\Security\Passwords;
use \Nette\Security\AuthenticationException;
use \Netteaddons\Security\Identity,
    \Netteaddons\Security\UserManager;

/**
 * Description of VerifyRepository
 *
 * @author karel.novak
 */
class VerifyRepository extends Repository {

    /**
     * 
     * @var \Nette\Security\Passwords
     */
    private $passwords = NULL;

    /**
     * 
     * @param \Nette\Security\Passwords $passwords
     * @return $this
     */
    public function setPasswords(Passwords $passwords) {
        $this->passwords = $passwords;
        return $this;
    }

    /**
     * 
     * @return \Nette\Security\Passwords
     */
    public function getPasswords(): Passwords {
        return ($this->passwords == NULL) ? new Passwords() : $this->passwords;
    }

    /**
     * 
     * @param \Nette\Database\Table\IRow $userRow
     * @return array
     */
    private function getUserRoles(IRow $userRow) {
        $roles = array();
        $resultRoles = $userRow
                ->related(UsersRepository::TABLE_USERROLES_NAME, UsersRepository::COLUMN_USERROLES_USERID)
                ->select(UsersRepository::COLUMN_USERROLES_ROLE)
                ->where(UsersRepository::COLUMN_USERROLES_ROLE . ' IS NOT NULL')
                ->order(UsersRepository::COLUMN_USERROLES_ORDER);
        foreach ($resultRoles as $rowRoles) {
            $roles[] = $rowRoles[UsersRepository::COLUMN_USERROLES_ROLE];
        }
        if (empty($roles)) {
            $roles[] = 'guest';
        }
        return $roles;
    }

    /**
     * 
     * @param string $username
     * @param string $password
     * @param string $otp
     * @return \Netteaddons\Security\Identity
     * @throws \Nette\Security\AuthenticationException
     */
    public function verifyUser($username, $password, $otp = NULL) {
        $passwords = new Passwords();
        $row = $this->context->table(UsersRepository::TABLE_USER_NAME)
                ->where(UsersRepository::COLUMN_USER_NAME, $username)
                ->where(UsersRepository::COLUMN_USER_EXPIRE_ACCOUNT . ' < NOW() OR ' . UsersRepository::COLUMN_USER_EXPIRE_ACCOUNT . ' IS NULL')
                ->where(UsersRepository::COLUMN_USER_DELETED, 0)
                ->where(UsersRepository::COLUMN_USER_DISABLED, 0)
                ->where(UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT . ' < ?', UserManager::MAX_WRONG_LOGIN)
                ->fetch();
        if (!$row) {
            throw new AuthenticationException('The username is incorrect.', IAuthenticator::IDENTITY_NOT_FOUND);
        } elseif (!$this->getPasswords()->verify($password, $row[UsersRepository::COLUMN_USER_PASSWORD_HASH])) {
            $row->update([
                UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT => new SqlLiteral('`' . UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT . '` + 1'),
            ]);
            throw new AuthenticationException('The password is incorrect.', IAuthenticator::INVALID_CREDENTIAL);
        } elseif (isset($row[UsersRepository::COLUMN_USER_KEY_OTP]) && !Google2FA::verify_key($row[UsersRepository::COLUMN_USER_KEY_OTP], $otp)) {
            $row->update([
                UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT => new SqlLiteral('`' . UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT . '` + 1'),
            ]);
            throw new AuthenticationException('The OTP key is incorrect.', IAuthenticator::INVALID_CREDENTIAL);
        } elseif ($this->getPasswords()->needsRehash($row[UsersRepository::COLUMN_USER_PASSWORD_HASH])) {
            $row->update([
                UsersRepository::COLUMN_USER_PASSWORD_HASH => $this->getPasswords()->hash($password),
            ]);
        }
        $row->update([
            UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT => 0,
        ]);
        $roles = $this->getUserRoles($row);
        $data = array();
        foreach ($row->toArray() as $key => $value) {
            if (
                    $key == UsersRepository::COLUMN_USER_PASSWORD_HASH ||
                    $key == UsersRepository::COLUMN_USER_KEY_OTP ||
                    $key == UsersRepository::COLUMN_USER_KEY_API ||
                    $key == UsersRepository::COLUMN_USER_EXPIRE_ACCOUNT ||
                    $key == UsersRepository::COLUMN_USER_EXIPIRE_PASSWORD ||
                    $key == UsersRepository::COLUMN_USER_DELETED ||
                    $key == UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT
            ) {
                continue;
            }
            $data[$key] = $value;
        }
        return new Identity($row[UsersRepository::COLUMN_USER_ID], $roles, $data);
    }

    /**
     * 
     * @param string $keyApi
     * @return \Netteaddons\Security\Identity
     * @throws \Nette\Security\AuthenticationException
     */
    public function verifyKeyApi($keyApi) {
        $row = $this->context->table(UsersRepository::TABLE_USER_NAME)
                ->where(UsersRepository::COLUMN_USER_DELETED, 0)
                ->where(UsersRepository::COLUMN_USER_DISABLED, 0)
                ->where(UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT . ' < ?', UserManager::MAX_WRONG_LOGIN)
                ->where(UsersRepository::COLUMN_USER_KEY_API, $keyApi)
                ->fetch();
        if (!$row) {
            throw new AuthenticationException('The API key is incorrect.', IAuthenticator::IDENTITY_NOT_FOUND);
        }
        $row->update([
            UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT => 0,
        ]);
        $roles = $this->getUserRoles($row);
        $data = array();
        foreach ($row->toArray() as $key => $value) {
            if (
                    $key == UsersRepository::COLUMN_USER_PASSWORD_HASH ||
                    $key == UsersRepository::COLUMN_USER_KEY_OTP ||
                    $key == UsersRepository::COLUMN_USER_KEY_API ||
                    $key == UsersRepository::COLUMN_USER_DELETED ||
                    $key == UsersRepository::COLUMN_USER_WRONG_LOGIN_COUNT
            ) {
                continue;
            }
            $data[$key] = $value;
        }
        return new Identity($row[UsersRepository::COLUMN_USER_ID], $roles, $data);
    }

    /**
     * 
     * @param \Nette\Security\Permission $permission
     */
    public function fetchPermissionRoles(Permission $permission) {
        $roles = array();
        $result = $this->getTable(RolesRepository::TABLE_ROLES_NAME);
        $result->select(RolesRepository::COLUMN_ROLES_NAME);
        $result->select(':' . RolesRepository::TABLE_ROLESPARENTS_NAME . '(' . RolesRepository::COLUMN_ROLESPARENTS_ROLE . ').' . RolesRepository::COLUMN_ROLESPARENTS_ROLE);
        $result->select(':' . RolesRepository::TABLE_ROLESPARENTS_NAME . '(' . RolesRepository::COLUMN_ROLESPARENTS_ROLE . ').' . RolesRepository::COLUMN_ROLESPARENTS_PARENT);
        $result->order(RolesRepository::COLUMN_ROLESPARENTS_ORDER);

        foreach ($result as $row) {
            if (!isset($roles[$row[RolesRepository::COLUMN_ROLES_NAME]])) {
                $roles[$row[RolesRepository::COLUMN_ROLES_NAME]] = array();
            }
            if (!empty($row[RolesRepository::COLUMN_ROLESPARENTS_ROLE])) {
                $roles[$row[RolesRepository::COLUMN_ROLES_NAME]] = array($row[RolesRepository::COLUMN_ROLESPARENTS_PARENT]);
            }
        }
        $permission->addResource(RolesRepository::RESOURCE_MANAGEMENT_ROLE);
        $permission->addResource(UsersRepository::RESOURCE_MANAGEMENT_USER);
        foreach ($roles as $role => $parents) {
            if (empty($parents)) {
                $permission->addRole($role);
            } else {
                $permission->addRole($role, $parents);
            }
            $permission->allow($role, RolesRepository::RESOURCE_MANAGEMENT_ROLE, $role);
        }
    }

    /**
     * 
     * @param \Nette\Security\Permission $permission
     */
    public function fetchPermissionResources(\Nette\Security\Permission $permission) {
        $resources = array();
        $result = $this->getTable(ResourcesRepository::TABLE_RESOURCES_NAME);
        $result->select(ResourcesRepository::COLUMN_RESOURCES_NAME);
        $result->select(':' . ResourcesRepository::TABLE_RESOURCESPARENTS_NAME . '(' . ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE . ').' . ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE);
        $result->select(':' . ResourcesRepository::TABLE_RESOURCESPARENTS_NAME . '(' . ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE . ').' . ResourcesRepository::COLUMN_RESOURCESPARENTS_PARENT);
        $result->order(ResourcesRepository::COLUMN_RESOURCESPARENTS_ORDER);

        foreach ($result as $row) {
            if (!isset($resources[$row[ResourcesRepository::COLUMN_RESOURCES_NAME]])) {
                $resources[$row[ResourcesRepository::COLUMN_RESOURCES_NAME]] = array();
            }
            if (!empty($row[ResourcesRepository::COLUMN_RESOURCESPARENTS_RESOURCE])) {
                $resources[$row[ResourcesRepository::COLUMN_RESOURCES_NAME]] = array($row[ResourcesRepository::COLUMN_RESOURCESPARENTS_PARENT]);
            }
        }
        foreach ($resources as $resource => $parents) {
            if (empty($parents)) {
                $permission->addResource($resource);
            } else {
                $permission->addResource($resource, $parents);
            }
        }
    }
}
