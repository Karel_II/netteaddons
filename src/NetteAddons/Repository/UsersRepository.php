<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use \Nette\Database\Context,
    \Nette\Database\SqlLiteral,
    \Nette\Database\UniqueConstraintViolationException,
    \Nette\Security\Passwords,
    \Nette\Security\User;
use \Netteaddons\Security\Identity,
    \Netteaddons\Security\Identities,
    \NetteAddons\UserNameDuplicateException;
use \Google2FA\Google2FA,
    \Base32\Base32;

/**
 * Description of UsersRepository
 *
 * @author karel.novak
 */
class UsersRepository extends Repository {

    use TRepositorySecurity;

    const
            TABLE_USER_NAME = '_securityUsers',
            COLUMN_USER_ID = 'id',
            COLUMN_USER_NAME = 'username',
            COLUMN_FIRST_NAME = 'firstName',
            COLUMN_MIDDLE_NAME = 'middleName',
            COLUMN_LAST_NAME = 'lastName',
            COLUMN_USER_PASSWORD_HASH = 'password',
            COLUMN_USER_KEY_OTP = 'keyOtp',
            COLUMN_USER_KEY_API = 'keyApi',
            COLUMN_USER_EXPIRE_ACCOUNT = 'expireAccount',
            COLUMN_USER_EXIPIRE_PASSWORD = 'expirePassword',
            COLUMN_USER_EMAIL = 'email',
            COLUMN_USER_PROFILE_IMAGE = 'profileImage',
            COLUMN_USER_WRONG_LOGIN_COUNT = 'wrongLoginCount',
            COLUMN_USER_DISABLED = 'disabled',
            COLUMN_USER_DELETED = 'deleted';
    const
            TABLE_USERROLES_NAME = '_securityUsersRoles',
            COLUMN_USERROLES_USERID = 'userId',
            COLUMN_USERROLES_ROLE = 'role',
            COLUMN_USERROLES_ORDER = 'orders';
    const
            TABLE_USERDYNAMIC_NAME = '_securityUsersDynamic',
            COLUMN_USERDYNAMIC_USERID = 'userId',
            COLUMN_USERDYNAMIC_KEY_OTP_PASSWORD_RECOVERY = 'keyOtpPasswordRecovery';
    const
            RESOURCE_MANAGEMENT_USER = 'resourceManagementUser',
            RESOURCE_MANAGEMENT_USER_PRIVILEDGE_ADD = 'resourceManagementUserPriviledgeAdd',
            RESOURCE_MANAGEMENT_USER_PRIVILEDGE_SELF = 'resourceManagementUserPriviledgeSelf',
            RESOURCE_MANAGEMENT_USER_PRIVILEDGE_OTHER = 'resourceManagementUserPriviledgeOther',
            RESOURCE_MANAGEMENT_USER_PRIVILEDGE_PASSWORD_RESET = 'resourceManagementUserPasswordReset';

    public function __construct(Context $context, User $securityUser) {
        parent::__construct($context);
        $this->setSecurityUser($securityUser);
    }

    /**
     * 
     *                          IDENTITY
     * 
     */

    /**
     * 
     * @param int $userId
     * @return boolean|Identity
     */
    public function getIdentity($userId) {
        if (!$this->rightsUserManageVerify($userId)) {
            return false;
        }
        $resultIdentity = $this->getTable(self::TABLE_USER_NAME)->
                select(self::COLUMN_USER_ID)->
                select(self::COLUMN_USER_NAME)->
                select(self::COLUMN_FIRST_NAME)->
                select(self::COLUMN_MIDDLE_NAME)->
                select(self::COLUMN_LAST_NAME)->
                select(self::COLUMN_USER_KEY_OTP)->
                select(self::COLUMN_USER_KEY_API)->
                select(self::COLUMN_USER_EMAIL)->
                where(self::COLUMN_USER_ID, $userId);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $data = $rowIdentity->toArray();
            $resultVisitors = $rowIdentity->related(VisitorsRepository::TABLE_VISITORS_NAME, VisitorsRepository::COLUMN_VISITORS_USERID)->
                    select('MAX(' . VisitorsRepository::COLUMN_VISITORS_DATETIME . ') AS lastLogin');
            if (($rowVisitors = $resultVisitors->fetch())) {
                $data['lastLogin'] = $rowVisitors['lastLogin'];
            }
            $identityRoles = array();
            $resultRoles = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    select(self::COLUMN_USERROLES_ROLE)->
                    select(self::COLUMN_USERROLES_ORDER)->
                    where(self::COLUMN_USERROLES_ROLE . ' IS NOT NULL');
            foreach ($resultRoles as $rowRole) {
                $identityRoles[$rowRole[self::COLUMN_USERROLES_ORDER]] = $rowRole[self::COLUMN_USERROLES_ROLE];
            }
            if (empty($identityRoles)) {
                $identityRoles[] = 'guest';
            }
            return new Identity($rowIdentity[self::COLUMN_USER_ID], $identityRoles, $data);
        }
        return false;
    }

    public function getIdentities() {
        $return = new Identities();
        if (!$this->rightsUserAddVerify()) {
            return false;
        }
        $resultIdentity = $this->getTable(self::TABLE_USER_NAME)->
                select(self::COLUMN_USER_ID)->
                select(self::COLUMN_USER_NAME)->
                select(self::COLUMN_FIRST_NAME)->
                select(self::COLUMN_MIDDLE_NAME)->
                select(self::COLUMN_LAST_NAME)->
                select(self::COLUMN_USER_KEY_OTP)->
                select(self::COLUMN_USER_KEY_API)->
                select(self::COLUMN_USER_EMAIL);
        foreach ($resultIdentity as $rowIdentity) {
            $data = $rowIdentity->toArray();
            $resultVisitors = $rowIdentity->related(VisitorsRepository::TABLE_VISITORS_NAME, VisitorsRepository::COLUMN_VISITORS_USERID)->
                    select('MAX(' . VisitorsRepository::COLUMN_VISITORS_DATETIME . ') AS lastLogin');
            if (($rowVisitors = $resultVisitors->fetch())) {
                $data['lastLogin'] = $rowVisitors['lastLogin'];
            }
            $identityRoles = array();
            $resultRoles = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    select(self::COLUMN_USERROLES_ROLE)->
                    select(self::COLUMN_USERROLES_ORDER)->
                    where(self::COLUMN_USERROLES_ROLE . ' IS NOT NULL');
            foreach ($resultRoles as $rowRole) {
                $identityRoles[$rowRole[self::COLUMN_USERROLES_ORDER]] = $rowRole[self::COLUMN_USERROLES_ROLE];
            }
            if (empty($identityRoles)) {
                $identityRoles[] = 'guest';
            }
            $identity = new Identity($rowIdentity[self::COLUMN_USER_ID], $identityRoles, $data);
            $return[] = $identity;
        }
        return $return;
    }

    /**
     * 
     * @param Identity $user
     * @return boolean
     */
    public function fetchIdentity(Identity $user) {
        if (!$this->rightsUserManageVerify($user->id)) {
            return false;
        }
        $resultIdentity = $this->getTable(self::TABLE_USER_NAME)->
                select(self::COLUMN_USER_ID)->
                select(self::COLUMN_USER_NAME)->
                select(self::COLUMN_FIRST_NAME)->
                select(self::COLUMN_MIDDLE_NAME)->
                select(self::COLUMN_LAST_NAME)->
                select(self::COLUMN_USER_KEY_OTP)->
                select(self::COLUMN_USER_KEY_API)->
                select(self::COLUMN_USER_EMAIL)->
                where(self::COLUMN_USER_ID, $user->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $data = $rowIdentity->toArray();
            $resultVisitors = $rowIdentity->related(VisitorsRepository::TABLE_VISITORS_NAME, VisitorsRepository::COLUMN_VISITORS_USERID)->
                    select('MAX(' . VisitorsRepository::COLUMN_VISITORS_DATETIME . ') AS lastLogin');
            if (($rowVisitors = $resultVisitors->fetch())) {
                $data['lastLogin'] = $rowVisitors['lastLogin'];
            }
            $identityRoles = array();
            $resultRoles = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    select(self::COLUMN_USERROLES_ROLE)->
                    select(self::COLUMN_USERROLES_ORDER)->
                    where(self::COLUMN_USERROLES_ROLE . ' IS NOT NULL');
            foreach ($resultRoles as $rowRole) {
                $identityRoles[$rowRole[self::COLUMN_USERROLES_ORDER]] = $rowRole[self::COLUMN_USERROLES_ROLE];
            }
            if (empty($identityRoles)) {
                $identityRoles[] = 'guest';
            }
            $user->roles = $identityRoles;
            foreach ($data as $key => $value) {
                $user->$key = $value;
            }
            return true;
        }
        return false;
    }

    /**
     * 
     * @param Identity $identity
     * @return boolean
     */
    public function identityUpdate(Identity $identity) {
        if (!$this->rightsUserManageVerify($identity->id)) {
            return false;
        }
        $data = $identity->getData();
        $update = array();
        $update[self::COLUMN_FIRST_NAME] = (empty($data[self::COLUMN_FIRST_NAME])) ? NULL : $data[self::COLUMN_FIRST_NAME];
        $update[self::COLUMN_MIDDLE_NAME] = (empty($data[self::COLUMN_MIDDLE_NAME])) ? NULL : $data[self::COLUMN_MIDDLE_NAME];
        $update[self::COLUMN_LAST_NAME] = (empty($data[self::COLUMN_LAST_NAME])) ? NULL : $data[self::COLUMN_LAST_NAME];
        $update[self::COLUMN_USER_KEY_OTP] = (empty($data[self::COLUMN_USER_KEY_OTP])) ? NULL : $data[self::COLUMN_USER_KEY_OTP];
        $update[self::COLUMN_USER_KEY_API] = (empty($data[self::COLUMN_USER_KEY_API])) ? NULL : $data[self::COLUMN_USER_KEY_API];
        $update[self::COLUMN_USER_EMAIL] = (empty($data[self::COLUMN_USER_EMAIL])) ? NULL : $data[self::COLUMN_USER_EMAIL];
        $this->getTable(self::TABLE_USER_NAME)->where(self::COLUMN_USER_ID, $identity->id)->update($update);
        return true;
    }

    /**
     * 
     * @param Identity $identity
     * @param string $password
     * @return boolean
     * @throws \Exception
     */
    public function identityUpdatePassword(Identity $identity, $password) {
        if (!$this->rightsUserManageVerify($identity->id)) {
            return false;
        }
        try {
            $this->context->table(self::TABLE_USER_NAME)->where(self::COLUMN_USER_ID, $identity->id)->update([
                self::COLUMN_USER_PASSWORD_HASH => Passwords::hash($password),
            ]);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * 
     * @param Identity $identity
     * @return boolean
     * @throws \Exception
     */
    public function identityAddOtpPasswordChange(Identity $identity) {
        if (!$this->rightsUserResetPassword()) {
            return false;
        }
        $otpInitalizationKey = Base32::base32_generate();     // Set the inital key
        $resultUser = $this->context->table(UsersRepository::TABLE_USER_NAME)->
                where(UsersRepository::COLUMN_USER_EMAIL, $identity->email);
        if (($rowUser = $resultUser->fetch())) {
            $rowUser->related(UsersRepository::TABLE_USERDYNAMIC_NAME)->delete();
            $rowUser->related(UsersRepository::TABLE_USERDYNAMIC_NAME)->insert([
                UsersRepository::COLUMN_USERDYNAMIC_KEY_OTP_PASSWORD_RECOVERY => $otpInitalizationKey,
            ]);
            $otpTimeStamp = Google2FA::get_timestamp();
            $otpSecretKey = Base32::base32_decode($otpInitalizationKey); // Decode it into binary
            return Google2FA::oath_hotp($otpSecretKey, $otpTimeStamp); // Get current token;
        }
        return false;
    }

    /**
     * 
     * @param Identity $identity
     * @param string $otp
     * @return boolean
     */
    public function identityFetchOtpPasswordChange(Identity $identity, $otp) {
        if (!$this->rightsUserResetPassword()) {
            return false;
        }
        $resultUser = $this->context->table(UsersRepository::TABLE_USER_NAME)->
                where(UsersRepository::COLUMN_USER_EMAIL, $identity->email);
        if (($rowUser = $resultUser->fetch())) {
            $resultDynamic = $rowUser->related(UsersRepository::TABLE_USERDYNAMIC_NAME)->
                    select(UsersRepository::COLUMN_USERDYNAMIC_KEY_OTP_PASSWORD_RECOVERY);
            if (($rowDynamic = $resultDynamic->fetch()) && Google2FA::verify_key($rowDynamic[UsersRepository::COLUMN_USERDYNAMIC_KEY_OTP_PASSWORD_RECOVERY], $otp)) {
                $identity->id = $rowUser[UsersRepository::COLUMN_USER_ID];
                $identity->username = $rowUser[UsersRepository::COLUMN_USER_NAME];
                return true;
            }
            $rowUser->related(UsersRepository::TABLE_USERDYNAMIC_NAME)->delete();
        }
        return false;
    }

    /**
     * 
     *                          USER
     * 
     */

    /**
     * Adds new user.
     * @param  string
     * @param  string
     * @param  string
     * @return void
     * @throws DuplicateNameException
     */
    public function addUser($username, $email, $password, $role = 'guest') {
        if (empty($username) || empty($password) || !$this->rightsUserAddVerify()) {
            return false;
        }
        try {
            $result = $this->context->table(self::TABLE_USER_NAME)->insert([
                self::COLUMN_USER_NAME => $username,
                self::COLUMN_USER_PASSWORD_HASH => Passwords::hash($password),
                self::COLUMN_USER_EMAIL => $email,
            ]);
            $result->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)
                    ->insert([
                        self::COLUMN_USERROLES_ROLE => $role,
                        self::COLUMN_USERROLES_ORDER => 0]);
        } catch (UniqueConstraintViolationException $e) {
            throw new UserNameDuplicateException;
        }
    }

    /**     * ************************************************************************
     *                              ROLES
     * ***************************************************************************** */

    /**
     * 
     * @param Identity $identity
     * @param string $role
     * @param integer $order
     * @return boolean
     */
    public function idtentityRoleAdd(Identity $identity, $role, $order = NULL) {
        if (!($this->rightsUserManageVerify($identity->id) && $this->rightsRoleVerify($role))) {
            return false;
        }
        $this->context->beginTransaction();
        $resultIdentity = $this->getTable()->
                select(self::COLUMN_USER_ID)->
                where(self::COLUMN_USER_ID, $identity->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $resultMinMax = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    select('IFNULL(MAX(' . self::COLUMN_USERROLES_ORDER . ')+1, 0) AS `order`')->
                    select('IFNULL(MIN(' . self::COLUMN_USERROLES_ORDER . '), 0) AS `min`');
            if (($rowMinMax = $resultMinMax->fetch())) {
                $order = (is_numeric($order) && $rowMinMax['order'] >= $order) ? $order : $rowMinMax['order'];
                $resultMoveBlock = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID);
                if ($rowMinMax['min'] > 0) {
                    $resultMoveBlock->
                            where(self::COLUMN_USERROLES_ORDER . ' <= ?', $order)->
                            update(array(self::COLUMN_USERROLES_ORDER => new SqlLiteral('`' . self::COLUMN_USERROLES_ORDER . '` - 1')));
                } else {
                    $resultMoveBlock->
                            where(self::COLUMN_USERROLES_ORDER . ' >= ?', $order)->
                            update(array(self::COLUMN_USERROLES_ORDER => new SqlLiteral('`' . self::COLUMN_USERROLES_ORDER . '` + 1')));
                }
            } else {
                $order = 0;
            }
            $insert = array();
            $insert[self::COLUMN_USERROLES_ROLE] = $role;
            $insert[self::COLUMN_USERROLES_ORDER] = $order;
            $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    insert($insert);
        } else {
            $this->context->rollBack();
            return false;
        }
        $this->context->commit();
        return true;
    }

    /**
     * 
     * @param Identity $identity
     * @param string $role
     * @return boolean
     */
    public function idtentityRoleDelete(Identity $identity, $role) {
        if (!($this->rightsUserManageVerify($identity->id) && $this->rightsRoleVerify($role))) {
            return false;
        }
        $order = NULL;
        $this->context->beginTransaction();
        $resultIdentity = $this->getTable()->
                select(self::COLUMN_USER_ID)->
                where(self::COLUMN_USER_ID, $identity->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $resultRole = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    select(self::COLUMN_USERROLES_ROLE)->
                    select(self::COLUMN_USERROLES_ORDER)->
                    where(self::COLUMN_USERROLES_ROLE, $role)->
                    where(self::COLUMN_USERROLES_ROLE . ' IS NOT NULL');
            if (($rowRole = $resultRole->fetch()) && isset($rowRole[self::COLUMN_USERROLES_ORDER])) {
                $order = $rowRole[self::COLUMN_USERROLES_ORDER];
                $rowRole->delete();
            } else {
                $this->context->rollBack();
                return false;
            }
            $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    where(self::COLUMN_USERROLES_ORDER . ' >= ?', $order)->
                    update(array(self::COLUMN_USERROLES_ORDER => new SqlLiteral('`' . self::COLUMN_USERROLES_ORDER . '` - 1')));
        } else {
            $this->context->rollBack();
            return false;
        }
        $this->context->commit();
        return true;
    }

    /**
     * 
     * @param Identity $identity
     * @param string $role
     * @param integer $order
     * @return boolean
     */
    public function idtentityRoleMove(Identity $identity, $role, $order = NULL) {
        if (!($this->rightsUserManageVerify($identity->id) && $this->rightsRoleVerify($role))) {
            return false;
        }
        $this->context->beginTransaction();
        $resultIdentity = $this->getTable()->
                select(self::COLUMN_USER_ID)->
                where(self::COLUMN_USER_ID, $identity->id);
        if (($rowIdentity = $resultIdentity->fetch())) {
            $resultRole = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                    select(self::COLUMN_USERROLES_ROLE)->
                    select(self::COLUMN_USERROLES_ORDER)->
                    where(self::COLUMN_USERROLES_ROLE, $role)->
                    where(self::COLUMN_USERROLES_ROLE . ' IS NOT NULL');
            if (($rowRole = $resultRole->fetch()) && isset($rowRole[self::COLUMN_USERROLES_ORDER])) {
                $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                        where(self::COLUMN_USERROLES_ORDER . ' >= ?', $rowRole[self::COLUMN_USERROLES_ORDER])->
                        where(self::COLUMN_USERROLES_ROLE . ' NOT LIKE', $role)->
                        update(array(self::COLUMN_USERROLES_ORDER => new SqlLiteral('`' . self::COLUMN_USERROLES_ORDER . '` - 1')));

                $resultMinMax = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID)->
                        select('IFNULL(MAX(' . self::COLUMN_USERROLES_ORDER . ')+1, 0) AS `order`')->
                        select('IFNULL(MIN(' . self::COLUMN_USERROLES_ORDER . '), 0) AS `min`');
                if (($rowMinMax = $resultMinMax->fetch())) {
                    $order = (is_numeric($order) && $rowMinMax['order'] >= $order) ? $order : $rowMinMax['order'];
                    $resultMoveBlock = $rowIdentity->related(self::TABLE_USERROLES_NAME, self::COLUMN_USERROLES_USERID);
                    if ($rowMinMax['min'] > 0) {
                        $resultMoveBlock->
                                where(self::COLUMN_USERROLES_ORDER . ' <= ?', $order)->
                                update(array(self::COLUMN_USERROLES_ORDER => new SqlLiteral('`' . self::COLUMN_USERROLES_ORDER . '` - 1')));
                    } else {
                        $resultMoveBlock->
                                where(self::COLUMN_USERROLES_ORDER . ' >= ?', $order)->
                                update(array(self::COLUMN_USERROLES_ORDER => new SqlLiteral('`' . self::COLUMN_USERROLES_ORDER . '` + 1')));
                    }
                } else {
                    $order = 0;
                }
                $rowRole->update(array(self::COLUMN_USERROLES_ORDER => $order));
            } else {
                $this->context->rollBack();
                return false;
            }
        } else {
            $this->context->rollBack();
            return false;
        }
        $this->context->commit();
        return true;
    }

}
