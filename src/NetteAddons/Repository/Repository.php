<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use Nette\Database\Context,
    Nette\Utils\Callback;

/**
 * Description of Repository
 *
 * @author karel.novak
 */
abstract class Repository {

    protected $storageEngine = 'INNODB';

    /** @var \Nette\Database\Connection */
    protected $connection;

    /** @var \Nette\Database\Context */
    protected $context;

    /** @var string|null */
    protected $prefix = NULL;

    /** @var string|null */
    protected $suffix = NULL;

    /** @var callable[]  function (string $tableName): void; Occurs after table not found is raised */
    public $onCreateTable;

    public function __construct(Context $context) {
        $this->context = $context;
        $this->connection = $context->getConnection();
        $this->connection->onQuery[] = [$this, 'handleQuery'];
    }

    /**
     * 
     * @param string $prefix
     * @return $this
     */
    public function setPrefix($prefix) {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * 
     * @param string $suffix
     * @return $this
     */
    public function setSuffix($suffix) {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * 
     * @param \NetteAddons\Repository\IRepositoryCreate $repositoryCreate
     */
    public function setRepositoryCreate(IRepositoryCreate $repositoryCreate) {
        $this->onCreateTable[] = Callback::check([$repositoryCreate, 'createTable']);
    }

    /**
     * 
     * @param \Nette\Database\Connection $connection
     * @param \Nette\Database\DriverException|\Nette\Database\ResultSet $result
     */
    public function handleQuery(\Nette\Database\Connection $connection, $result) {
        if ($result instanceof \Nette\Database\DriverException) {
            $info = $result->errorInfo;
            $matches = array();
            if (
                    ($this->onCreateTable && $this->onCreateTable !== NULL && $info[0] == 'HY000' && $info[1] == '1' && preg_match('/^no\ssuch\stable:\s(?P<tableName>.*)$/', $info[2], $matches)) ||
                    ($this->onCreateTable && $this->onCreateTable !== NULL && $info[0] == '42S02' && $info[1] == '1146' && preg_match('/^Table\s\'(?P<tableName>.*)\'\sdoesn\'t\sexist$/', $info[2], $matches))
            ) {
                foreach ($this->onCreateTable as $callback) {
                    call_user_func($callback, $matches['tableName']);
                }
            }
        }
    }

    /**
     * Vrací název databáze odvozené z třídy , předpony a přípony
     * @param string|null $name Název Databáze nebo třídy
     * @param string|null $prefix Předpona
     * @param string|null $suffix Přípona
     * @return string
     * @throws \Exception
     */
    public static function tableName(?string $name = NULL, ?string $prefix = NULL, ?string $suffix = NULL): string {
        $m = array();
        if ($name === NULL && preg_match('#(\w+)(Repository|RepositoryLite)$#', get_called_class(), $m)) {
            $tableName = lcfirst($m[1]);
        } elseif ($name !== NULL && preg_match('#(\w+)(Repository|RepositoryLite)$#', $name, $m)) {
            $tableName = lcfirst($m[1]);
        } elseif ($name !== NULL) {
            $tableName = $name;
        } else {
            throw new \Exception('Wrong $name input !');
        }
        return (($prefix === NULL) ? $tableName : $prefix . ucfirst($tableName)) . (($suffix === NULL) ? '' : ucfirst($suffix));
    }

    /**
     * Vrací název databáze odvozené z třídy
     * @return string
     */
    protected function getTableName($tableName = NULL) {
        if ($tableName === NULL) {
            $m = array();
            preg_match('#(\w+)(Repository|RepositoryLite)$#', get_class($this), $m);
            $tableName = lcfirst($m[1]);
        }
        return (($this->prefix === NULL) ? $tableName : $this->prefix . ucfirst($tableName)) . (($this->suffix === NULL) ? '' : ucfirst($this->suffix));
    }

    /**
     * Vrací objekt reprezentující databázovou tabulku.
     * @param string $tableName
     * @return Table\Selection
     * @throws \Nette\InvalidArgumentException
     */
    protected function getTable($tableName = NULL) {
        $tableName = $this->getTableName($tableName);
        try {
            return $this->context->table($tableName);
        } catch (\Nette\InvalidArgumentException $exception) {
            $message = $exception->getMessage();
            $matches = array();
            if ($this->onCreateTable !== NULL && preg_match('/^Table\s\'(?P<tableName>.*)\'\sdoes\snot\sexist.$/', $message, $matches)) {
                foreach ($this->onCreateTable as $callback) {
                    call_user_func($callback, $matches['tableName']);
                }
                $this->context->getStructure()->rebuild();
            } else {
                throw $exception;
            }
        }
        return $this->context->table($tableName);
    }

    /**
     * 
     * @param int $value
     * @param string $tableName
     */
    protected function setAutoIncrement($value = 1, $tableName = NULL) {
        $tableName = $this->getTableName($tableName);
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            $this->connection->query('ALTER TABLE ' . $tableName . ' AUTO_INCREMENT = ?;', $value);
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            $this->context->query('UPDATE SQLITE_SEQUENCE SET seq = ? WHERE name = ' . $tableName, $value);
        }
    }

    /*     * *******************************************************************
     *                             TABLE OPERATIONS
     * ********************************************************************** */

    /**
     * 
     * @param array $insert
     * @param type $tableName
     */
    protected function insertIgnore(array $insert, $tableName = NULL) {
        $tableName = $this->getTableName($tableName);
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            $this->context->query("INSERT IGNORE INTO " . $tableName, $insert);
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            $this->context->query("INSERT OR IGNORE INTO " . $tableName, $insert);
        }
    }

    public function truncate($tableName = NULL) {
        if (!isset($tableName)) {
            $tableName = $this->getTableName();
        }
        $this->connection->query('TRUNCATE ' . $tableName);
    }

    /*     * *******************************************************************
     *                                    HELPERS
     * ********************************************************************** */

    /**
     * 
     * @param string $reference
     * @param string $columns
     * @param boolean $addColumnRefeence
     * @param Repository $selfPrefixSuffix
     * @return string
     */
    protected static function getReferenceSelect($reference, $columns, $addColumnRefeence = FALSE, Repository $selfPrefixSuffix = NULL) {
        $return = array();
        foreach ($columns as $column) {
            $newColumnName = ($addColumnRefeence) ? lcfirst(str_replace(['.', ':'], '', ucwords($reference, '.:'))) . ucfirst($column) : $column;
            $return[] = (($selfPrefixSuffix !== NULL) ? ((($selfPrefixSuffix->prefix === NULL) ? $reference : $selfPrefixSuffix->prefix . ucfirst($reference)) . (($selfPrefixSuffix->suffix === NULL) ? '' : ucfirst($selfPrefixSuffix->suffix))) : $reference) . '.' . $column . ' AS ' . $newColumnName;
        }
        return implode(', ', $return);
    }

}
