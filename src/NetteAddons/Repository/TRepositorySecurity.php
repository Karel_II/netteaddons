<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use Nette\Security\IAuthorizator;
use Nette\Security\User;
use NetteAddons\UserEditNotAlowed,
    NetteAddons\RoleEditNotAlowed;

/**
 *
 * @author karel.novak
 * 
 * @property-write \Nette\Security\User  $securityUser Description
 */
trait TRepositorySecurity {

    /**
     *
     * @var \Nette\Security\User 
     */
    protected $securityUser = NULL;

    private function setSecurityUser(User $securityUser) {
        $this->securityUser = $securityUser;
    }

    protected function rightsRoleVerify($role) {
        if (!empty($role) && $this->securityUser->isAllowed(RolesRepository::RESOURCE_MANAGEMENT_ROLE, $role)) {
            return TRUE;
        }
        throw new RoleEditNotAlowed;
    }

    protected function rightsUserResetPassword() {
        if (
                $this->securityUser->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_PASSWORD_RESET)
        ) {
            return TRUE;
        }
        throw new UserEditNotAlowed;
    }

    protected function rightsUserManageVerify($userId) {
        if (!empty($userId) && (
                (
                $this->securityUser->id !== $userId &&
                $this->securityUser->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_OTHER)
                ) ||
                $this->securityUser->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_SELF)
                )
        ) {
            return TRUE;
        }
        throw new UserEditNotAlowed;
    }

    protected function rightsUserAddVerify() {
        if ($this->securityUser->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_ADD)) {
            return TRUE;
        }
        throw new UserEditNotAlowed;
    }

    /**
     * Has a user effective access to the Resource?
     * If $resource is null, then the query applies to all resources.
     * @param  string  resource
     * @param  string  privilege
     * @return bool
     */
    public function isAllowed($resource = IAuthorizator::ALL, $privilege = IAuthorizator::ALL) {
        return $this->securityUser->isAllowed($resource, $privilege);
    }

    /**
     * Return id of logged user
     * @return int|null
     */
    protected function getUserId($default = NULL) {
        return($this->securityUser->isLoggedIn()) ? $this->securityUser->id : $default;
    }

}
