<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

use \Nette\Database\Context,
    \Nette\Security\User;

/**
 * Description of ResourcesRepository
 *
 * @author Karel
 */
class ResourcesRepository extends Repository {

    const
            TABLE_RESOURCES_NAME = '_securityResources',
            COLUMN_RESOURCES_NAME = 'name';
    const
            TABLE_RESOURCESPARENTS_NAME = '_securityResourcesParents',
            COLUMN_RESOURCESPARENTS_RESOURCE = 'resource',
            COLUMN_RESOURCESPARENTS_PARENT = 'parent',
            COLUMN_RESOURCESPARENTS_ORDER = 'orders';

    /**
     * 
     * @param \Nette\Security\Permission $permission
     * @param string $resource
     * @param string $parents
     */
    private static function addResource(\Nette\Security\Permission $permission, $resource, $parents) {
        if (empty($parents)) {
            $permission->addResource($resource);
        } else {
            $permission->addResource($resource, $parents);
        }
    }

    /**
     * 
     * @param \Nette\Security\Permission $permission
     */
    public function fetchPermissionResources(\Nette\Security\Permission $permission) {
        $resources = array();
        $result = $this->getTable(self::TABLE_RESOURCES_NAME);
        $result->select(self::COLUMN_RESOURCES_NAME);
        $result->select(':' . self::TABLE_RESOURCESPARENTS_NAME . '.' . self::COLUMN_RESOURCESPARENTS_ROLE);
        $result->select(':' . self::TABLE_RESOURCESPARENTS_NAME . '.' . self::COLUMN_RESOURCESPARENTS_PARENT);
        $result->order(self::COLUMN_RESOURCESPARENTS_LEVEL);

        foreach ($result as $row) {
            if (!isset($resources[$row[self::COLUMN_RESOURCES_NAME]])) {
                $resources[$row[self::COLUMN_RESOURCES_NAME]] = array();
            }
            if (!empty($row[self::COLUMN_RESOURCESPARENTS_ROLE])) {
                $resources[$row[self::COLUMN_RESOURCES_NAME]] = array($row[self::COLUMN_RESOURCESPARENTS_PARENT]);
            }
        }
        foreach ($resources as $resource => $parents) {
            self::addResource($permission, $resource, $parents);
        }
    }

    public function getResources() {
        $resources = array();
        $result = $this->getTable(TABLE_RESOURCES_NAME)->select(self::COLUMN_RESOURCES_NAME);
        foreach ($result as $row) {
            $resources[] = $row;
        }
        return $resources;
    }

}
