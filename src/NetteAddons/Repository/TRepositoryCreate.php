<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Repository;

/**
 *
 * @author karel.novak
 */
trait TRepositoryCreate {

    /**
     * 
     * @param string $tableName
     */
    public abstract function createTable($tableName = NULL);

    protected function alterAddColumn($tableName = NULL, $columnName = NULL, $columnType = NULL) {
        try {
            $this->context->query("ALTER TABLE `" . $tableName . "` ADD `" . $columnName . "` " . $columnType . ";");
        } catch (\PDOException $e) {
            $info = $e->errorInfo;
            if ($info[0] == 'HY000' && $info[1] == '1' && $info[2] == 'duplicate column name: ' . $columnName) {
                return TRUE;
            } else {
                throw new \PDOException($e->getMessage());
            }
        }
        return TRUE;
    }

    protected function getUnsigned() {
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            return 'UNSIGNED ';
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            return '';
        }
    }

    protected function addKey($type, $name, $columns) {
        if (!preg_match('/^(PRIMARY|PRIMARY_AUTO_INCREMENT|INDEX|UNIQUE|SPATIAL|FULLTEXT)$/', $type)) {
            throw new \Exception();
        }
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            switch ($type) {
                case 'PRIMARY_AUTO_INCREMENT':
                    return "PRIMARY KEY `" . $name . "` (`" . implode('`,`', $columns) . "`)";
                case 'INDEX':
                    return "KEY `" . $name . "` (`" . implode('`,`', $columns) . "`)";
                case 'FULLTEXT':
                    return $type . " `" . $name . "` (`" . implode('`,`', $columns) . "`)";
                default:
                    return $type . " KEY `" . $name . "` (`" . implode('`,`', $columns) . "`)";
            }
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            switch ($type) {
                case 'PRIMARY':
                    return "PRIMARY KEY (`" . implode('`,`', $columns) . "`)";
                case 'PRIMARY_AUTO_INCREMENT':
                case 'INDEX':
                case 'SPATIAL':
                    return FALSE;
                default:
                    return $type . " (`" . implode('`,`', $columns) . "`)";
            }
        } else {
            throw new \Exception;
        }
    }

    protected function getColumnPrimaryKey($autoincrement = FALSE) {
        if ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\MySqlDriver) {
            return 'AUTO_INCREMENT ';
        } elseif ($this->connection->getSupplementalDriver() instanceof \Nette\Database\Drivers\SqliteDriver) {
            return (($autoincrement) ? 'PRIMARY KEY AUTOINCREMENT' : '');
        }
    }

}
