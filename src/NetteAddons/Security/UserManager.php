<?php

namespace Netteaddons\Security;

use Nette;
use NetteAddons\Repository\VerifyRepository;
use \Nette\Security\AuthenticationException;

/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator {

    const
            PASSWORD_MIN_LENGTH = 7,
            MAX_WRONG_LOGIN = 20,
            USER_DEFINED_ACCOUNTS = 1000;

    /**
     * @var NetteAddons\Repository\VerifyRepository 
     */
    private $verifyRepository;

    public function __construct(VerifyRepository $verifyRepository) {

        $this->verifyRepository = $verifyRepository;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials) {
        list($username, $serializedSecret) = $credentials;
        $secret = unserialize($serializedSecret);
        if (isset($username) && isset($secret["password"])) {
            return $this->verifyRepository->verifyUser($username, $secret["password"], $secret["otp"]);
        }
        if (isset($username) && isset($secret["keyApi"])) {
            return $this->verifyRepository->verifyKeyApi($secret["keyApi"]);
        }
        throw new AuthenticationException('Unknown method');
    }

}
