<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Netteaddons\Security;

use NetteAddons\Repository;
use Nette;
use NetteAddons\Repository\VerifyRepository;

/**
 * Description of AuthorizatorFactory
 *
 * @author karel.novak
 */
class AuthorizatorFactory {

    /**
     * @var NetteAddons\Repository\VerifyRepository 
     */
    private $verifyRepository;

    /**
     *
     * @var Nette\Security\Permission; 
     */
    private $permission = NULL;

    public function __construct(VerifyRepository $verifyRepository) {
        $this->verifyRepository = $verifyRepository;
    }

    protected function localPermission(\Nette\Security\Permission $permission) {
        return $permission;
    }

    /** @return Nette\Security\Permission */
    public function create() {
        $this->permission = new Nette\Security\Permission;
        $this->verifyRepository->fetchPermissionResources($this->permission);
        $this->verifyRepository->fetchPermissionRoles($this->permission);

        /*
         * USER RIGHTS
         */

        $this->permission->allow('root', Repository\RolesRepository::RESOURCE_MANAGEMENT_ROLE, Nette\Security\IAuthorizator::ALL); //root can asigned all roles
        $this->permission->allow('root', Repository\UsersRepository::RESOURCE_MANAGEMENT_USER, Nette\Security\IAuthorizator::ALL); //root can edit all users

        $this->permission->allow('admin', Repository\UsersRepository::RESOURCE_MANAGEMENT_USER, Repository\UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_OTHER); //admin can edit all users
        $this->permission->allow('user', Repository\UsersRepository::RESOURCE_MANAGEMENT_USER, Repository\UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_SELF); //user can edit only self
        $this->permission->allow('admin', Repository\UsersRepository::RESOURCE_MANAGEMENT_USER, Repository\UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_ADD); //admin can add another user

        $this->permission->allow('admin', Repository\UsersRepository::RESOURCE_MANAGEMENT_USER, Repository\UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_PASSWORD_RESET);

        $this->localPermission($this->permission);
        return $this->permission;
    }

}
