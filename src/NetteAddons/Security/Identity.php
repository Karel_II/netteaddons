<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Netteaddons\Security;

use Nette;

/**
 * Description of Identity
 *
 * @author karel.novak
 * 
 * @property-read string $displayName Description
 */
class Identity extends Nette\Security\Identity implements \NetteAddons\Utils\IObjectId {

    public function getDisplayName() {
        if ($this->data['firstName'] || $this->data['middleName'] || $this->data['lastName']) {
            return ($this->data['firstName'] ? $this->data['firstName'] . ' ' : '' ) . ($this->data['middleName'] ? $this->data['middleName'] . ' ' : '' ) . ($this->data['lastName'] ? $this->data['lastName'] : '' );
        } else {
            return ucfirst($this->data['username']);
        }
    }

    /**
     * Return true if Object id has been set !
     * @return bool
     */
    public function hasId() {
        return isset($this->id);
    }

    /**
     * 
     * @param type $id
     * @return \static
     */
    public static function newObjectId($id) {
        $return = new static;
        $return->setId($id);
        return $return;
    }

    /**
     * 
     * @return string
     */
    public function __toString() {
        return (string) $this->getDisplayName();
    }

}
