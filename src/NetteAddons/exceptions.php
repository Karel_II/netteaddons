<?php

namespace NetteAddons;

class ObjectIdException extends \Exception {

    protected $message = 'Object id must be scalar !';

    public function __construct() {
        parent::__construct();
    }

}

class ObjectDeleteException extends \Exception {

    protected $message = 'Object delete must be boolean or integer !';

    public function __construct() {
        parent::__construct();
    }

}

class UserNameDuplicateException extends \Exception {
    
}

class UserEditNotAlowed extends \Exception {
    
}

class RoleEditNotAlowed extends \Exception {
    
}
