<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        /**
         * 'NetteAddons
         */
        'NetteAddons\\ObjectIdException' => 'exceptions.php',
        'NetteAddons\\ObjectDeleteException' => 'exceptions.php',
        /**
         * 'NetteAddons\\UI
         */
        'NetteAddons\\Application\\UI\\BaseComponentControl' => 'UI/BaseComponentControl.php',
        'NetteAddons\\Application\\UI\\BaseFormFactory' => 'UI/BaseFormFactory.php',
        'NetteAddons\\Application\\UI\\ISetupFormFactory' => 'UI/ISetupFormFactory.php',
        'NetteAddons\\Application\\UI\\SetupFormFactory' => 'UI/SetupFormFactory.php',
        'NetteAddons\\Application\\UI\\IChangePasswordFormFactory' => 'UI/IUsersManageForms.php',
        'NetteAddons\\Application\\UI\\IChangeRolesFormFactory ' => 'UI/IUsersManageForms.php',
        'NetteAddons\\Application\\UI\\IChangeUserFormFactory ' => 'UI/IUsersManageForms.php',
        'NetteAddons\\Application\\UI\\IForgotPasswordFormFactory ' => 'UI/IUsersManageForms.php',
        'NetteAddons\\Application\\UI\\ISignInFormFactory ' => 'UI/IUsersManageForms.php',
        'NetteAddons\\Application\\UI\\ISignUpFormFactory ' => 'UI/IUsersManageForms.php',
        /**
         * 'NetteAddons\\Http
         */
        'NetteAddons\\Http\\Url' => 'Http/Url.php',
        /**
         * 'NetteAddons\\Utils
         */
        'NetteAddons\\Utils\\ArrayHashObject' => 'Utils/ArrayHashObject.php',
        'NetteAddons\\Utils\\IObjectId' => 'Utils/IObjectId.php',
        'NetteAddons\\Utils\\TObjectId' => 'Utils/TObjectId.php',
        'NetteAddons\\Utils\\IObjectDelete' => 'Utils/IObjectDelete.php',
        'NetteAddons\\Utils\\TObjectDelete' => 'Utils/TObjectDelete.php',
        'NetteAddons\\Utils\\IObjectArray' => 'Utils/IObjectArray.php',
        'NetteAddons\\Utils\\TObjectArray' => 'Utils/TObjectArray.php',
        'NetteAddons\\Utils\\Image' => 'Utils/Image.php',
        'NetteAddons\\Utils\\Math' => 'Utils/Math.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/NetteAddons/' . $classMap[$className];
    }
});
